# Generated by Django 5.0.2 on 2024-02-29 12:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0007_remove_task_assigned_to_remove_task_column_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='column',
            name='project',
        ),
        migrations.DeleteModel(
            name='Task',
        ),
        migrations.DeleteModel(
            name='Column',
        ),
    ]
