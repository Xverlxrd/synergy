from django.db import models
from django.utils import timezone


class UserProject(models.Model):
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    project = models.ForeignKey('Project', on_delete=models.CASCADE)

class Project(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    start_date = models.DateField(auto_now_add=True)
    end_date = models.DateField(blank=True, null=True)
    isEnd = models.BooleanField(blank=False, null=True)
    created_by = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='created_projects')
    members = models.ManyToManyField('users.User', through='UserProject', related_name='user_projects') 

    def __str__(self):
        return self.name
    
class Column(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='columns')
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Task(models.Model):
    PRIORITY_CHOICES = [
        ('low', 'Не важно'),
        ('medium', 'Средне'),
        ('high', 'Важно'),
    ]

    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='tasks', null=True, blank=True)
    column = models.ForeignKey(Column, on_delete=models.CASCADE, related_name='tasks', null=True, blank=True)
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateField(auto_now_add=True)
    due_date = models.DateField(blank=True, null=True)
    assigned_to = models.ForeignKey('users.User', on_delete=models.CASCADE, null=True, blank=True)
    completed = models.BooleanField(default=False)
    priority = models.CharField(max_length=10, choices=PRIORITY_CHOICES, default='low')

    def __str__(self):
        return self.title