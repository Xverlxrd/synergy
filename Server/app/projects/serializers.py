from rest_framework import serializers
from .models import Column, Project, Task
from users.models import User

class ProjectSerializer(serializers.ModelSerializer):
    members = serializers.PrimaryKeyRelatedField(many=True, queryset=User.objects.all())

    def validate_name(self, value):
        existing_projects = Project.objects.filter(name=value)
        if existing_projects.exists():
            raise serializers.ValidationError("Проект с таким именем уже существует.")
        return value
    
    class Meta:
        model = Project
        fields = '__all__'
        extra_kwargs = {
            'start_date': {'read_only': True},
        }

class ColumnSerializer(serializers.ModelSerializer):
    class Meta:
        model = Column
        fields = '__all__'

class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = '__all__'