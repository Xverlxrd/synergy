from django.http import Http404
from django.shortcuts import get_object_or_404
from .serializers import ColumnSerializer, ProjectSerializer, TaskSerializer
from .models import Column, Project, Task, UserProject
from rest_framework import generics
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.permissions import IsAuthenticated
from .permissions import IsColumnOwnerInProject, IsProjectCreator
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

# Create your views here.
class ProjectListAPIView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        projects = Project.objects.all()
        serializer = ProjectSerializer(projects, many=True)
        return Response(serializer.data)

class ProjectCreateAPIView(generics.CreateAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = (IsAuthenticated, )

    def perform_create(self, serializer):
        project = serializer.save(created_by=self.request.user)
        self.request.user.projects.add(project)

class ProjectRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = (IsAuthenticated, )


class ColumnListCreateView(generics.ListCreateAPIView):
    serializer_class = ColumnSerializer
    permission_classes = [IsAuthenticated, ]  


    def get_queryset(self):
        project_id = self.kwargs.get('project_id')
        project = get_object_or_404(Project, id=project_id)
        return Column.objects.filter(project=project)

    def perform_create(self, serializer):
        project_id = self.kwargs.get('project_id')
        project = get_object_or_404(Project, id=project_id)
        serializer.save(project=project)

class ColumnRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Column.objects.all()
    serializer_class = ColumnSerializer
    permission_classes = [IsAuthenticated, ]  

    def perform_update(self, serializer):
        # Дополнительные действия, если необходимо
        serializer.save()

    def perform_destroy(self, instance):
        # Дополнительные действия, если необходимо
        instance.delete()

class TaskListView(generics.ListAPIView):
    serializer_class = TaskSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        # Получаем ID проекта из параметра запроса
        project_id = self.kwargs.get('project_id')
        
        # Получаем проект или возвращаем 404, если проект не существует
        project = get_object_or_404(Project, id=project_id)

        # Возвращаем все задачи, принадлежащие данному проекту
        return Task.objects.filter(project=project)

class TaskCreateView(generics.CreateAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    def create(self, request, *args, **kwargs):
        project_id = request.data.get('project')
        column_id = request.data.get('column')

        # Проверка, что проект существует
        project = get_object_or_404(Project, id=project_id)

        # # Проверка, что текущий пользователь - создатель проекта
        # if request.user != project.created_by:
        #     return Response({"detail": "Вы не можете создавать задачи в этом проекте, так как не являетесь его создателем."}, status=status.HTTP_403_FORBIDDEN)

        # Проверка, что колонка принадлежит указанному проекту
        if not Column.objects.filter(project=project, id=column_id).exists():
            return Response({"detail": "Указанная колонка не принадлежит проекту."}, status=status.HTTP_400_BAD_REQUEST)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
    
class TaskRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = (IsAuthenticated, )

    def get_object(self):
        task_id = self.kwargs.get('pk')
        return get_object_or_404(Task, id=task_id)