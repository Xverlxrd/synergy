"""
URL configuration for app project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from chat.views import MessageListCreateAPIView
from notification.views import NotificationListCreateAPIView, NotificationRetrieveUpdateDestroyAPIView
from projects.views import ColumnListCreateView, ColumnRetrieveUpdateDestroyAPIView, ProjectCreateAPIView, ProjectListAPIView, ProjectRetrieveUpdateDestroyAPIView, TaskCreateView, TaskListView, TaskRetrieveUpdateDestroyAPIView
from users.views import *
from django.conf import settings
from django.conf.urls.static import static
from rest_framework import routers
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/add_contact/', AddContactAPIView.as_view(), name='add_contact'),
    path('api/remove_contact/', RemoveContactAPIView.as_view(), name='remove_contact'),
    path('api/users/', UserAPIList.as_view()),
    path('api/users/<int:pk>/', UserAPIUpdate.as_view()),
    path('api/usersdetail/<int:pk>/', UserAPIDetailView.as_view()),
    path('api/login/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/login/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/login/verify/', TokenVerifyView.as_view(), name='token_verify'),
    path('api/logout/', LogoutAPIView.as_view()),
    path('api/projects/', ProjectListAPIView.as_view(), name='project-list'),
    path('api/projects/create/', ProjectCreateAPIView.as_view(), name='create_project'),
    path('api/projects/<int:pk>/', ProjectRetrieveUpdateDestroyAPIView.as_view(), name='retrieve-update-destroy-project'),
    path('api/projects/<int:project_id>/columns/', ColumnListCreateView.as_view(), name='column-list-create'),
    path('api/columns/<int:pk>/', ColumnRetrieveUpdateDestroyAPIView.as_view(), name='column-detail'),
    path('api/projects/<int:project_id>/tasks/', TaskListView.as_view(), name='task-list'),
    path('api/tasks/create', TaskCreateView.as_view(), name='create-task'),
    path('api/tasks/<int:pk>/', TaskRetrieveUpdateDestroyAPIView.as_view(), name='task-detail'),
    path('api/notifications/', NotificationListCreateAPIView.as_view(), name='notification-list'),
    path('api/notifications/<int:pk>/', NotificationRetrieveUpdateDestroyAPIView.as_view(), name='notification-detail'),
    path('api/', include('chat.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)