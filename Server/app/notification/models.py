from django.db import models

# Create your models here.
class Notification(models.Model):
    users = models.ManyToManyField('users.User', related_name='notifications')
    project = models.ForeignKey('projects.Project', on_delete=models.CASCADE, null=True, blank=True)
    members = models.ManyToManyField('users.User', related_name='notifications_as_member', null=True, blank=True)
    message = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey('users.User', on_delete=models.CASCADE, blank=True, null=True)
    is_read = models.BooleanField(default=False)
    is_system = models.BooleanField(default=False)

    def __str__(self):
        return self.message