from django.urls import path
from .views import MessageListCreateAPIView, MessageRetrieveUpdateDestroyAPIView


urlpatterns = [
    path('messages/', MessageListCreateAPIView.as_view(), name='message-list-create'),
    path('messages/<int:pk>/', MessageRetrieveUpdateDestroyAPIView.as_view(), name='message-detail'),

]

