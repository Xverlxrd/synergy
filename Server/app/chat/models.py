from django.db import models

# Create your models here.
class Message(models.Model):
    author = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='sent_messages')
    recipient  = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='received_messages', null=True)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.content
