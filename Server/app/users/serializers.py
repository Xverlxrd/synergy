from rest_framework import serializers
from django.contrib.auth.password_validation import validate_password
from .models import User
from django.contrib.auth import get_user_model
from rest_framework_simplejwt.tokens import RefreshToken, TokenError
from django.contrib.auth.hashers import make_password
from projects.serializers import ProjectSerializer



class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True)
    email = serializers.EmailField(required=True)
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    skills = serializers.CharField(required=False)
    job_title = serializers.CharField(required=False)
    work_experience = serializers.CharField(required=False)
    about = serializers.CharField(required=False)
    image = serializers.ImageField(required=False)
    projects = ProjectSerializer(many=True, read_only=True)

    
    class Meta:
        model = User
        fields = '__all__'
        extra_kwargs = {'password': {'write_only': True}}
    
    def create(self, validated_data):
        validated_data['password'] = make_password(validated_data.get('password'))
        return super().create(validated_data)
    
    def update(self, instance, validated_data):
        instance.email = validated_data.get("email", instance.email)
        instance.username = validated_data.get("username", instance.username)
        instance.first_name = validated_data.get("first_name", instance.first_name)
        instance.last_name = validated_data.get("last_name", instance.last_name)
        instance.skills = validated_data.get("skills", instance.skills)
        instance.job_title = validated_data.get("job_title", instance.job_title)
        instance.work_experience = validated_data.get("work_experience", instance.work_experience)
        instance.about = validated_data.get("about", instance.about)
        instance.image = validated_data.get("image", instance.image)

        password = validated_data.get("password")
        if password:
            instance.set_password(password)
        
        instance.save()
        return instance

class LogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField()


    def validate(self, attr):
        self.token = attr['refresh']
        return attr
    
    def save(self, **kwargs):
        try:
            token = RefreshToken(self.token)
            token.blacklist()
        except TokenError:
            self.fail('Плохой токен')