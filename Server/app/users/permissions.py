from rest_framework import permissions

class IsAdminOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method is permissions.SAFE_METHODS:
            return True
        
        return bool(request.user and request.user.is_staff)
    
class IsOwnerOrAdmin(permissions.BasePermission):


    def has_object_permission(self, request, view, obj):
        return obj.id == request.user.id or request.user.is_staff
