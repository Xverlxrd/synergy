from django.utils import timezone
from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import AbstractUser
from projects.models import UserProject, Project

# Create your models here.
class User(AbstractUser):
    image = models.ImageField(upload_to='user_images', blank=True, null=True, default='user_images/default_image.png')    
    contacts = models.ManyToManyField('self', symmetrical=True, blank=True)
    about = models.TextField(blank=True)
    skills = models.TextField(blank=True)
    job_title = models.CharField(max_length=255, blank=True)
    work_experience = models.TextField(blank=True)
    projects = models.ManyToManyField('projects.Project', related_name='project_members')

    def __str__(self):
        return self.first_name