from django.forms import model_to_dict
from django.shortcuts import get_object_or_404, render
from rest_framework import status


from .permissions import IsAdminOrReadOnly, IsOwnerOrAdmin
from .models import User
from rest_framework import generics, viewsets
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated
from rest_framework.views import APIView
from .serializers import UserSerializer, LogoutSerializer
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.decorators import api_view, permission_classes
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
class UserAPIList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserAPIUpdate(generics.RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = (IsOwnerOrAdmin, )

class UserAPIDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = (IsOwnerOrAdmin, )


class LogoutAPIView(generics.GenericAPIView):
    serializer_class = LogoutSerializer
    permission_classes = (IsAuthenticated, )

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response('Вы успешно разлогинились')
    

class AddContactAPIView(APIView):
    permission_classes = (IsAuthenticated, )

    def post(self, request, *args, **kwargs):
        user_id_to_add = request.data.get('user_id_to_add')

        # Проверяем, что передан идентификатор пользователя
        if not user_id_to_add:
            return Response({'error': 'Не указан идентификатор пользователя для добавления в контакты.'}, status=status.HTTP_400_BAD_REQUEST)

        # Получаем текущего пользователя
        current_user = request.user

        # Получаем пользователя, которого нужно добавить в контакты
        user_to_add = get_object_or_404(User, id=user_id_to_add)

        # Проверяем, что пользователь не пытается добавить самого себя
        if current_user == user_to_add:
            return Response({'error': 'Нельзя добавить самого себя в контакты.'}, status=status.HTTP_400_BAD_REQUEST)

        # Добавляем пользователя в контакты
        current_user.contacts.add(user_to_add)
        current_user.save()

        # Возвращаем успешный ответ
        serializer = UserSerializer(current_user)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
class RemoveContactAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        user_id_to_remove = request.data.get('user_id_to_remove')

        # Проверяем, что передан идентификатор пользователя
        if not user_id_to_remove:
            return Response({'error': 'Не указан идентификатор пользователя для удаления из контактов.'}, status=status.HTTP_400_BAD_REQUEST)

        # Получаем текущего пользователя
        current_user = request.user

        # Получаем пользователя, которого нужно удалить из контактов
        user_to_remove = get_object_or_404(User, id=user_id_to_remove)

        # Проверяем, что пользователь не пытается удалить самого себя
        if current_user == user_to_remove:
            return Response({'error': 'Нельзя удалить самого себя из контактов.'}, status=status.HTTP_400_BAD_REQUEST)

        # Удаляем пользователя из контактов
        current_user.contacts.remove(user_to_remove)
        current_user.save()

        # Возвращаем успешный ответ
        serializer = UserSerializer(current_user)
        return Response(serializer.data, status=status.HTTP_200_OK)