import React, {useEffect, useState} from 'react';
import Container from "@/components/Container/Container";
import NavBar from "@/components/NavBar/NavBar";
import AuthPage from "@/pages/AuthPage/AuthPage";
import {Route, Routes, useNavigate, useLocation} from "react-router-dom";
import RegistrationPage from "@/pages/RegistrationPage/RegistrationPage";
import {setAuthToken} from "@/helpers/utils/axiosConfig";
import {AuthService} from "@/helpers/utils/authService";
import {message, Modal} from "antd";
import {checkTokenExpiration} from "@/helpers/utils/tokenUtils";
import SettingsPage from "@/pages/SettingsPage/SettingsPage";
import classes from './style.module.scss'
import HeaderContent from "@/components/Content/HeaderContent/HeaderContent";
import {useDispatch, useSelector} from "react-redux";
import {setUser} from "@/app/store/actions/userActions";
import {RootState} from "@/app/store/reducers/rootReducer";
import BoardPage from "@/pages/BoardPage/BoardPage";
import ProjectPage from "@/pages/ProjectPage/ProjectPage";
import ContactsPage from "@/pages/ContactsPage/ContactsPage";
import ProjectInfo from "@/pages/ProjectInfo/ProjectInfo";
import Notification from "@/components/Notification/Notification";
import ChatPage from "@/pages/ChatPage/ChatPage";
import PageLoadTest from "@/tests/PageLoadTest";
const App = () => {
    const [openNotifications, setOpenNotifications] = useState(false)
    const navigate = useNavigate()
    const [modal, contextHolder] = Modal.useModal();
    const token =  localStorage.getItem('access_token')
    const user = useSelector((state: RootState) => state.user.user);
    const dispatch = useDispatch()
    const location = useLocation();

    useEffect(() => {
        if(token) {
            AuthService.me()
                .then((response) => {
                    dispatch(setUser(response.data))
                })
                .catch(() => {
                    message.error('Не удалось получить информацию о пользователе!')
                })
        }
    }, [])

    useEffect(() => {
        if(token) {
            setAuthToken(token)
            checkTokenExpiration(setUser, navigate,modal);
            const tokenCheckInterval = setInterval(() => checkTokenExpiration(setUser, navigate,modal), 800000);
            return () => clearInterval(tokenCheckInterval);
        }
    }, [token])

    // Тест на время загрузки первой страницы
    /**
    const handleLoaded = (loadTime: number) => {
        console.log(`Время загрузки: ${loadTime.toFixed(2)} мс`);
    };
     **/

    return (
        <Container>
            <NavBar />
            {/*<PageLoadTest onLoaded={handleLoaded} />*/}
            <div className={classes.content}>
                {user !== null && (
                    <HeaderContent openNotifications={openNotifications} setOpenNotifications={setOpenNotifications}/>
                )}
                <div className={`${location.pathname === '/board' ? classes.container_board : classes.container}`}>
                    {openNotifications && (
                        <Notification/>
                    )}
                    <Routes>
                        {token !== null ? (
                            <Route path='/' element={<SettingsPage/>}/>
                        ) : (
                            <Route path='/' element={<AuthPage/>}/>
                        )}

                        {token !== null ? (
                            <>
                                <Route path='/settings' element={<SettingsPage/>}/>
                                <Route path='/board' element={<BoardPage/>}/>
                                <Route path='/project' element={<ProjectPage/>}/>
                                <Route path='/contacts' element={<ContactsPage/>}/>
                                <Route path='/info' element={<ProjectInfo/>}/>
                                <Route path='/chat' element={<ChatPage/>}/>
                            </>
                        ) : (
                            <>
                                <Route path='/login' element={<AuthPage/>}/>
                                <Route path='/registration' element={<RegistrationPage/>}/>
                            </>
                        )}
                    </Routes>
                </div>
            </div>
        </Container>
    );
};

export default App;