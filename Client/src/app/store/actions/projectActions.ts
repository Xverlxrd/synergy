import {TProject} from "@/helpers/types/projectType";

export const SET_PROJECT = 'SET_PROJECT';

export const setProject = (project: TProject | null) => ({
    type: SET_PROJECT,
    payload: project,
});