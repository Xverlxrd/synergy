import {TUser} from "@/helpers/types/authTypes";

export const SET_USER = 'SET_USER';

export const setUser = (user: TUser | null) => ({
    type: SET_USER,
    payload: user,
});