
export const SET_NAVREFRESH = 'SET_NAVREFRESH';

export const setNavbarRefresh = (navbarRefresh: boolean) => ({
    type: SET_NAVREFRESH,
    payload: navbarRefresh,
});