import {TProject} from "@/helpers/types/projectType";
import {SET_PROJECT} from "@/app/store/actions/projectActions";

export interface ProjectState {
    project: TProject | null;
}

const initialState: ProjectState = {
    project: null,
};

const userReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case SET_PROJECT:
            return {
                ...state,
                project: action.payload,
            };
        default:
            return state;
    }
};

export default userReducer;