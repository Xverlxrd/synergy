import userReducer, {UserState} from "@/app/store/reducers/userReducer";
import {combineReducers} from "redux";
import projectReducer, {ProjectState} from "@/app/store/reducers/projectReducer";
import navbarReducer, {NavBarState} from "@/app/store/reducers/navbarReducer";

export interface RootState {
    user: UserState;
    project: ProjectState;
    navbarRefresh: NavBarState
}

const rootReducer = combineReducers({
    user: userReducer,
    project: projectReducer,
    navbarRefresh: navbarReducer,
});

export default rootReducer;