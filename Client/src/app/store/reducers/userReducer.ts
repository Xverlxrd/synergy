import {SET_USER} from "@/app/store/actions/userActions";
import {TUser} from "@/helpers/types/authTypes";

export interface UserState {
    user: TUser | null;
}

const initialState: UserState = {
    user: null,
};

const userReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case SET_USER:
            return {
                ...state,
                user: action.payload,
            };
        default:
            return state;
    }
};

export default userReducer;