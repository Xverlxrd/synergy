import {SET_NAVREFRESH} from "@/app/store/actions/navbarActions";

export interface NavBarState {
    navbarRefresh: boolean;
}

const initialState: NavBarState = {
    navbarRefresh: false,
};

const navBarReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case SET_NAVREFRESH:
            return {
                ...state,
                navbarRefresh: action.payload,
            };
        default:
            return state;
    }
};

export default navBarReducer;