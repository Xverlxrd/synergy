import React, {useState} from 'react';
import classes from './style.module.scss'
import {Button, Checkbox, Form, Input, message} from "antd";
import {LockOutlined, UserOutlined} from "@ant-design/icons";
import {Link, useNavigate} from "react-router-dom";
import {TAuthData, TUser, TUserData} from "@/helpers/types/authTypes";
import {AuthService} from "@/helpers/utils/authService";
import {setAuthToken, setRefreshToken} from "@/helpers/utils/axiosConfig";
import {useDispatch, useSelector} from "react-redux";
import {setUser} from "@/app/store/actions/userActions";
import {RootState} from "@/app/store/reducers/rootReducer";


const AuthPage = () => {
    const [form] = Form.useForm();
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const user = useSelector((state: RootState) => state.user.user);
    const onFinish = (values: TAuthData) => {
        setLoading(true);
        AuthService.login(values.username, values.password)
            .then((response) => {
                const { access, refresh } = response.data;
                setAuthToken(access);
                setRefreshToken(refresh)
                message.success('Вы успешно вошли в аккаунт!')
                navigate('/settings')
            })
            .catch((error) => {
                message.error('Что-то пошло не так. Пожалуйста, попробуйте еще раз.');
            })
            .finally(() => {
                setLoading(false);
                AuthService.me()
                    .then((response) => {
                        dispatch(setUser(response.data))
                    })
                    .catch(() => {
                        message.error('Не удалось получить информацию о пользователе!')
                    })
            });
    };

    return (
        <div className={classes.auth_container}>
            <div className={classes.auth_form_container}>
                <Form
                    onFinish={onFinish}
                    form={form}
                    name="normal_login"
                    className={classes.auth_form}
                    initialValues={{ remember: true }}
                >
                    <h1 className={classes.auth_form_title}>Войти в учетную запись</h1>
                    <Form.Item
                        name="username"
                        rules={[{ required: true, message: 'Пожалуйста введите логин!' }]}
                    >
                        <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Логин" />
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[{ required: true, message: 'Пожалуйста введите пароль!' }]}
                    >
                        <Input.Password
                            prefix={<LockOutlined className="site-form-item-icon" />}
                            type="password"
                            placeholder="Пароль"
                        />
                    </Form.Item>
                    <Form.Item>
                        <Form.Item name="remember" valuePropName="checked" noStyle>
                            <Checkbox>Запомнить</Checkbox>
                        </Form.Item>
                    </Form.Item>

                    <Form.Item >
                        <Button loading={loading} type="primary" htmlType="submit">
                            Войти
                        </Button>
                        <Link to={'/registration'} className={classes.auth_form_reg}>У вас нет учетной записи?</Link>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );
};

export default AuthPage;