import React, {useState} from 'react';
import classes from './style.module.scss'
import {Button, Form, Input, message, Modal} from "antd";
import {ProjectService} from "@/helpers/utils/projectService";
import FormItem from "antd/es/form/FormItem";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "@/app/store/reducers/rootReducer";
import {AuthService} from "@/helpers/utils/authService";
import {setUser} from "@/app/store/actions/userActions";

const ProjectPage = () => {
    const [openModal, setOpenModal] = useState(false)
    const [form] = Form.useForm();
    const user = useSelector((state: RootState) => state.user.user)
    const dispatch = useDispatch()

    const openModalHandler = () => {
        setOpenModal(true)
    }

    const closeModalHandler = () => {
        setOpenModal(false)
    }
    const me = () => {
        AuthService.me()
            .then(response => {
                dispatch(setUser(response.data))
            })
            .catch(() => {
                message.error('Ошибка получения данных пользователя!')
            })
    }

    const onOk = () => {
        const formData = {
            ...form.getFieldsValue(),
            created_by: user?.id,
            members: [],
        };

        ProjectService.createProject(formData)
            .then((response) => {
                message.success("Проект создан");
                closeModalHandler()
                me()
            })
            .catch((error) => {
                console.log(error);
            });
    };

    return (
        <div className={classes.project_container}>
            <Button className='primary-blue' onClick={openModalHandler}>Создать проект</Button>
            <Modal
                open={openModal}
                onCancel={closeModalHandler}
                onOk={onOk}
            >
                <Form
                    form={form}
                    layout='vertical'
                >
                    <FormItem label='Название' name='name'>
                        <Input/>
                    </FormItem>
                    <FormItem label='Описание' name='description'>
                        <Input/>
                    </FormItem>
                    <FormItem label='Дедлайн' name='end_date'>
                        <Input/>
                    </FormItem>
                </Form>
            </Modal>
        </div>
    );
};

export default ProjectPage;