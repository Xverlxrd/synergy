import React, {useEffect, useState} from 'react';
import classes from './style.module.scss'
import {ProjectService} from "@/helpers/utils/projectService";
import {useSelector} from "react-redux";
import {RootState} from "@/app/store/reducers/rootReducer";
import {Button, DatePicker, Form, Input, message, Modal, Popconfirm, Select, Tabs, TabsProps} from "antd";
import {TTask} from "@/helpers/types/projectType";
import {DeleteOutlined, EditOutlined, Loading3QuartersOutlined, LoadingOutlined} from "@ant-design/icons";
import PieDiagram from "@/components/Diagrams/PieDiagram/PieDiagram";
import AreaDiagram from "@/components/Diagrams/AreaDiagram/AreaDiagram";
import moment from "moment";
import {AuthService} from "@/helpers/utils/authService";
import {TUser} from "@/helpers/types/authTypes";
import FormItem from "antd/es/form/FormItem";
import {Option} from "antd/lib/mentions";
import {Simulate} from "react-dom/test-utils";
import cancel = Simulate.cancel;



const items: TabsProps['items'] = [
    {
        key: '1',
        label: 'За день',
    },
    {
        key: '2',
        label: 'За неделю',
    },
    {
        key: '3',
        label: 'За месяц',
    },
    {
        key: '4',
        label: 'За год',
    },
];
const ProjectInfo = () => {
    const project = useSelector((state: RootState) => state.project.project);
    const [tasks, setTasks] = useState<TTask[] | null>(null)
    const user = useSelector((state: RootState) => state.user.user)
    const [users, setUsers] = useState<TUser[] | null>(null)
    const [form] = Form.useForm()
    const [newTab, setNewTab] = useState<number>(1)

    const getTasks = () => {
        ProjectService.getTask(project?.id)
            .then(response => {
                setTasks(response.data)
            })
            .catch(error => {
                message.error(error)
            })
    }
    const isCompletedTask = (id_task: number, completed: boolean) => {
        if (id_task) {
            const formData = {
                ...form.getFieldsValue(),
                completed: completed
            };


            ProjectService.updateTask(id_task, formData)
                .then(response => {
                    message.success("Статус задачи изменен!")
                    getTasks()
                })
                .catch(error => {
                    message.error(error.response)
                })
        }
    }

    const deleteTask = (task_id: number) => {
        ProjectService.deleteTask(task_id)
            .then(() => {
                getTasks()
            })
            .catch(() => {
                getTasks()
                message.error('Не удалось удалить задачу!')
            })
    }
    const assign_user = () => {
        AuthService.allUsers()
            .then(response => {
                setUsers(
                    response.data.filter(member => project?.members.includes(member.id))
                )
            })
            .catch(response => {
                message.error('Не удалось получить список пользователей!')
            })
    }

    const changeTabs = (key: string) => {
        setNewTab(Number(key))
    };

    useEffect(() => {
        if (project) {
            assign_user()
            getTasks()
        }
    }, [project]);

    const sortedTasks = tasks?.sort((a, b) => b.id - a.id)

    return (
        <div className={classes.container}>
            <div className={classes.tasks}>
                <div className={classes.tasks_header}>
                    <Tabs defaultActiveKey="1" items={items} onChange={changeTabs} />
                </div>
                <div className={classes.tasks_list}>
                    {sortedTasks !== null && sortedTasks?.map(task => (
                        <div key={task.id} className={classes.tasks_item}>
                            <p className={classes.tasks_item_title}>{task.title}</p>
                            <div className={classes.tasks_item_body}>
                                <p className={classes.tasks_item_created}>
                                    Дата создания: {moment(task.created_at).format('DD.MM.YYYY')}
                                </p>
                                <div className={classes.tasks_item_info}>
                                    <div className={classes.tasks_item_user}>
                                        {users?.map(asignUser => asignUser.id === task.assigned_to && (
                                            <div key={asignUser.id}>
                                                <img className={classes.tasks_item_img} src={asignUser?.image}
                                                     alt={'Image'}/>
                                                <p className={classes.tasks_item_name}>
                                                    {asignUser?.first_name} {asignUser?.last_name}
                                                </p>
                                            </div>
                                        ))}
                                    </div>
                                    <div className={classes.tasks_item_task}>
                                        <LoadingOutlined
                                            className={!task.completed
                                                ?
                                                classes.tasks_item_notload
                                                :
                                                classes.tasks_item_load
                                        }/>
                                        <Popconfirm
                                            title="Удалить задачу"
                                            description="Вы действительно хотите удалить задачу?"
                                            onConfirm={() => deleteTask(task.id)}
                                            okText="Удалить"
                                            cancelText="Отмена"
                                        >
                                            <DeleteOutlined className={classes.tasks_item_remove}/>
                                        </Popconfirm>
                                        <Button
                                            onClick={() => isCompletedTask(task.id, !task.completed)}
                                            className={task.completed
                                                ?
                                                classes.tasks_item_completed
                                                :
                                                classes.tasks_item_work
                                        }>
                                            {
                                                task.completed
                                                    ?
                                                    'Выполнено'
                                                    :
                                                    'Не выполнено'
                                            }
                                        </Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>

            <div className={classes.diagrams}>

                <div className={classes.diagrams_area}>
                    <AreaDiagram tab={newTab} tasks={tasks}/>
                </div>
                <div className={classes.diagrams_pie}>
                    <PieDiagram tab={newTab} tasks={tasks}/>
                    <div className={classes.diagrams_pie_tooltips}>
                        <div className={classes.diagrams_pie_status}>
                            <Loading3QuartersOutlined className={classes.tasks_item_notload}/>
                            <p>В работе</p>
                        </div>
                        <div className={classes.diagrams_pie_status}>
                            <Loading3QuartersOutlined className={classes.tasks_item_load}/>
                            <p>Выполнено</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProjectInfo;