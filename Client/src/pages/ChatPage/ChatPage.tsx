import React, {useEffect, useState} from 'react';
import {Input, message} from 'antd';
import {AuthService} from '@/helpers/utils/authService';
import {TUser} from '@/helpers/types/authTypes';
import {useSelector} from 'react-redux';
import {RootState} from '@/app/store/reducers/rootReducer';
import Chat from '@/components/Chat/Chat';
import classes from './style.module.scss';
import {MessageService} from "@/helpers/utils/messageService";
import {TMessage} from "@/helpers/types/messageTypes";
import moment from "moment/moment";

const ChatPage = () => {
    const [chats, setChats] = useState<TUser[]>([]);
    const [selectedChat, setSelectedChat] = useState<TUser | null>(null);
    const [socket, setSocket] = useState<WebSocket | null>(null);
    const [messages, setMessages] = useState<TMessage[]>([]);
    const project = useSelector((state: RootState) => state.project.project);

    const user = useSelector((state: RootState) => state.user.user);

    useEffect(() => {
        AuthService.allUsers()
            .then(response => {
                const usersChats = response.data.filter(user => project?.members?.includes(user.id))
                if (user?.id !== project?.created_by) {
                    usersChats.push(response.data.filter(created => created.id === project.created_by)[0])
                    setChats(
                        usersChats
                    );
                } else {
                    setChats(
                        usersChats
                    );
                }
            })
            .catch(error => {
                message.error('Не удалось получить список чатов!');
            });

        return () => {
            if (socket) {
                socket.close();
            }
        };
    }, [user, project]);

    useEffect(() => {
        loadMessagesForChat()
    }, [selectedChat]);

    const loadMessagesForChat = () => {
        if (selectedChat) {
            MessageService.allMessages()
                .then(response => {
                    setMessages(response.data);
                })
                .catch(error => {
                    message.error('Не удалось получить сообщения');
                });
        }
    };
    useEffect(() => {
        setSelectedChat(null)
    }, [project]);

    useEffect(() => {
        if (socket) {
            socket.onmessage = function (event) {
                const receivedMessage = JSON.parse(event.data);
                loadMessagesForChat()
            };

            return () => {
                socket.onmessage = null;
            };
        }
    }, [socket]);

    const connectToChat = () => {
        if (socket) {
            socket.close();
        }

        const newSocket = new WebSocket(`ws://synergy-br94.onrender.com/ws/chat/`);
        setSocket(newSocket);
    };

    const sendMessage = (content: string) => {
        if (!socket || !selectedChat || !user) return;

        const messageData = {
            content: content,
            recipient: selectedChat?.id,
            author: user?.id
        };
        const wwMessageData = {
            message: content,
            recipient: selectedChat?.id,
            author: user?.id
        };

        MessageService.createMessage(messageData)
            .then(response => {
                socket.send(JSON.stringify(wwMessageData));
                loadMessagesForChat();
            })
            .catch(() => {
                message.error('Не удалось отправить сообщения');
            });
    };

    const filteredMessages = messages.filter(message =>
        (message.author === user?.id && message.recipient === selectedChat?.id)
        ||
        (message.author === selectedChat?.id && message.recipient === user?.id)
    )
    const sortedMessages = filteredMessages?.sort((a, b) => {
        return moment(a.timestamp).valueOf() - moment(b.timestamp).valueOf();
    });

    return (
        <div className={classes.chat_container}>
            <div className={classes.chats}>
                <div className={classes.chats_list}>
                    {chats.map(chat => (
                        <div
                            key={chat?.id}
                            className={`${classes.chats_item} 
                                ${selectedChat?.id === chat?.id
                                ? classes.chats_item_seleted
                                : ''
                            }`}
                            onClick={() => {
                                setSelectedChat(chat);
                                connectToChat();
                            }}
                        >
                            <img className={classes.chats_item_img} src={chat.image} alt='User_Avatar'/>
                            <p className={classes.chats_item_name}>{chat.first_name} {chat.last_name}</p>
                        </div>
                    ))}
                </div>
            </div>
            {selectedChat && <Chat
                loadMessagesForChat={loadMessagesForChat}
                sendMessage={sendMessage}
                messages={sortedMessages}
                selectedChat={selectedChat}
            />}
        </div>
    );
};

export default ChatPage;
