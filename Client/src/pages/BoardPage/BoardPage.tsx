import React, {useEffect, useState} from 'react';
import classes from './style.module.scss';
import {
    BarChartOutlined,
    BorderOutlined,
    CalendarOutlined,
} from "@ant-design/icons";
import {Badge, BadgeProps, Calendar, GetProp, Menu, MenuProps, message} from "antd";
import {useSelector} from "react-redux";
import {RootState} from "@/app/store/reducers/rootReducer";
import {ProjectService} from "@/helpers/utils/projectService";
import {TColumn, TTask} from "@/helpers/types/projectType";
import Board from "@/components/Board/Board";
import {Gantt} from "gantt-task-react";
import moment from "moment";


type MenuItem = GetProp<MenuProps, 'items'>[number];

function getItem(
    label: React.ReactNode,
    key?: React.Key | null,
    icon?: React.ReactNode,
    children?: MenuItem[],
): MenuItem {
    return {
        key,
        icon,
        children,
        label,
    } as MenuItem;
}

const itemsView: MenuItem[] = [
    getItem(
        <BorderOutlined className={classes.board_header_viewitem}/>, 1
    ),
    getItem(
        <CalendarOutlined className={classes.board_header_viewitem}/>, 2
    ),
];





const BoardPage = () => {
    const project = useSelector((state: RootState) => state.project.project)
    const [columns, setColumns] = useState<TColumn[] | null>(null)
    const [tasks, setTasks] = useState<TTask[] | null>(null)
    const [updateColumn, setUpdateColumn] = useState(false)
    const [board, setBoard] = useState('1')

    const updateTasks = () => {
        if (project !== null) {
            ProjectService.getTask(project?.id)
                .then((response) => {
                    setTasks(response.data);
                })
                .catch((error) => {
                    message.error(error);
                });
        }
    };


    useEffect(() => {
        if (project !== null) {
            ProjectService.getColumns(project?.id)
                .then((response) => {
                    setColumns(response.data)
                })
                .catch((error) => {
                    message.error(error)
                })

            updateTasks()
        }

    }, [project, updateColumn]);

    const selectBoardMenu: MenuProps['onClick'] = (e) => {
        setBoard(e.key)
    };

    const getListData = (value: any) => {
        const dateString = value.format('YYYY-MM-DD');
        const currentDayTasks = tasks?.filter(task => moment(task.due_date).format('YYYY-MM-DD') === dateString);
        return currentDayTasks?.map(task => ({
            type: task.completed ? 'success' : 'error' as BadgeProps['status'],
            content: task.title
        })) || [];
    };

    const dateCellRender = (value: any) => {
        const listData = getListData(value);
        return (
            <ul className={classes.events}>
                {listData.map(item => (
                    <li key={item.content}>
                        <Badge status={item.type} text={item.content} />
                    </li>
                ))}
            </ul>
        );
    };

    return (
        <div className={classes.board_container}>
            <div className={classes.board_header}>
                <Menu
                    className={classes.board_header_view}
                    mode='horizontal'
                    style={{ width: 256 }}
                    defaultSelectedKeys={['1']}
                    items={itemsView}
                    onClick={selectBoardMenu}
                />
            </div>
            {board === '1' && (
                <Board
                    tasks={tasks}
                    columns={columns}
                    updateColumn={updateColumn}
                    setUpdateColumn={setUpdateColumn}
                    updateTasks={updateTasks}
                />
            )}
            {board === '2' && (
                <Calendar
                    className={classes.calendar}
                    dateCellRender={dateCellRender}
                />
            )}
        </div>
    );
};

export default BoardPage;