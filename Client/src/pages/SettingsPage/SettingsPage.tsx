import React, {useState} from 'react';
import classes from "./style.module.scss";
import {Menu, MenuProps} from "antd";
import {
    BellOutlined, ProjectOutlined,
    QuestionOutlined,
    SettingOutlined,
    UserOutlined
} from "@ant-design/icons";
import {getItem} from "@/pages/SettingsPage/type";
import ProfileContent from "@/components/Content/SettingsContent/ProfileContent/ProfileContent";
import ProfileEdit from "@/components/Content/SettingsContent/ProfileContent/ProfileEdit/ProfileEdit";
import NotificationContent from "@/components/Content/SettingsContent/NotificationContent/NotificationContent";
import AccountContent from "@/components/Content/SettingsContent/AccountContent/AccountContent";
import HelpContent from "@/components/Content/SettingsContent/HelpContent/HelpContent";
import {useSelector} from "react-redux";
import {RootState} from "@/app/store/reducers/rootReducer";
import ProjectContent from "@/components/Content/SettingsContent/ProjectContent/ProjectContent";



const items: MenuProps['items'] = [
    getItem('Профиль', 'profile', <UserOutlined />),
    getItem('Уведомления', 'notification', <BellOutlined />),
    getItem('Аккаунт', 'account', <SettingOutlined />),
    getItem('Проект', 'project', <ProjectOutlined />),
    getItem('Помощь', 'help', <QuestionOutlined />),
];

const SettingsPage = () => {
    const [activeMenu, setActiveMenu] = useState('profile')
    const user = useSelector((state: RootState) => state.user.user);

    const onClick: MenuProps['onClick'] = (e) => {
        setActiveMenu(e.key)
    };

    const settingContentRender = (menu:string) => {
        switch (menu) {
            case 'profile':
                return <ProfileContent user={user} setActiveMenu={setActiveMenu}/>
            case 'profile_edit':
                return <ProfileEdit user={user} setActiveMenu={setActiveMenu}/>
            case 'notification':
                return <NotificationContent/>
            case 'account':
                return <AccountContent/>
            case 'project':
                return <ProjectContent/>
            case 'help':
                return <HelpContent/>
            default:
                return <ProfileContent user={user} setActiveMenu={setActiveMenu}/>
        }
    }

    return (
        <div className={classes.setting_container}>
            <Menu
                className={classes.setting_menu}
                onClick={onClick}
                defaultSelectedKeys={['profile']}
                mode="inline"
                items={items}
            />
            {settingContentRender(activeMenu)}
        </div>
    );
};

export default SettingsPage;