import React, {useEffect, useState} from 'react';
import classes from './style.module.scss'
import {AuthService} from "@/helpers/utils/authService";
import {TUser} from "@/helpers/types/authTypes";
import {Input, message} from "antd";
import {UserAddOutlined, UserDeleteOutlined} from "@ant-design/icons";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "@/app/store/reducers/rootReducer";
import {SET_USER, setUser} from "@/app/store/actions/userActions";
import ProfileContent from "@/components/Content/SettingsContent/ProfileContent/ProfileContent";
const ContactsPage = () => {
    const [users, setUsers] = useState(null)
    const user = useSelector((state: RootState) => state.user.user);
    const dispatch = useDispatch()
    const [selectUser, setSelectUser] = useState<TUser | null>(null)
    const checkUserInfo = (userData: TUser, user_id: number) => {
        if (selectUser === null) {
            setSelectUser(userData)
        } else if (user_id !== selectUser.id) {
            setSelectUser(userData)
        } else {
            setSelectUser(null)
        }
    }

    const me = () => {
        AuthService.me()
            .then(response => {
                dispatch(setUser(response.data))
            })
            .catch(() => {
                message.error('Ошибка получения данных пользователя!')
            })
    }

    useEffect(() => {
        AuthService.allUsers()
            .then((response) => {
                setUsers(response.data)
            })
            .catch(error => {
                message.error(`Произошла ошибка: ${error}`)
            })
    }, []);

    const addUser = (user_id_to_add: number) => {
        AuthService.addUser(user_id_to_add)
            .then(response => {
                message.success('Пользователь добавлен!')
                me()
            })
            .catch(error => {
                message.error('Пользователь не добавлен, произошла ошибка!')
            })
    }
    const deleteUser = (user_id_to_remove: number) => {
        AuthService.deleteUser(user_id_to_remove)
            .then(response => {
                message.success('Пользователь удален!')
                me()
            })
            .catch(error => {
                message.error('Пользователь не удален, произошла ошибка!')
            })
    }




    return (
        <div className={classes.contacts_container}>
            <div className={classes.contacts}>
                <div className={classes.contacts_list}>
                    <Input className={classes.contacts_list_input}/>
                    {users &&
                        users
                            .filter((contact: TUser) => contact.id !== user?.id) // Исключаем текущего пользователя
                            .map((contact: TUser) => (
                                <div className={classes.contacts_user} key={contact.id}>
                                    <div onClick={() => checkUserInfo(contact, contact.id)} className={classes.contacts_user_mininfo}>
                                        <img className={classes.contacts_user_img} src={contact.image} alt='Аватарка' />
                                        <p className={classes.contacts_user_name}>
                                            {contact.first_name} {contact.last_name}
                                        </p>
                                    </div>
                                    <div className={classes.contacts_user_btns}>
                                        {user?.contacts.includes(contact.id) ? (
                                            <UserDeleteOutlined
                                            onClick={() => deleteUser(contact.id)}
                                                className={classes.contacts_user_notadd}
                                            />
                                        ) : (
                                            <UserAddOutlined
                                                onClick={() => addUser(contact.id)}
                                                className={classes.contacts_user_add}
                                            />
                                        )}
                                    </div>
                                </div>
                            ))}
                </div>
                <div className={classes.contacts_find}>
                    <p>Найдено</p>
                    <span className={classes.contacts_find_count}>{users?.length}</span>
                    <p>пользователя</p>
                </div>
            </div>
            {selectUser !== null && (
                <ProfileContent
                    user={selectUser}
                    addUser={addUser}
                    deleteUser={deleteUser}
                />
            )}
        </div>
    );
};

export default ContactsPage;