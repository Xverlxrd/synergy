import React, {useState} from 'react';
import classes from "./style.module.scss";
import {Button, Form, Input, message} from "antd";
import {Link, useNavigate} from "react-router-dom";
import {TRegData} from "@/helpers/types/authTypes";
import {AuthService} from "@/helpers/utils/authService";

const RegistrationPage = () => {
    const [form] = Form.useForm();
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate()
    const onFinish = (values: TRegData) => {
        setLoading(true);

        AuthService.registration(values.username, values.password, values.first_name, values.last_name, values.email)
            .then((response) => {
                message.success('Вы успешно зарегистрировались!')
                navigate('/')
            })
            .catch((error) => {
                message.error('Ошибка при регистрации!');
            })
            .finally(() => {
                setLoading(false);
            });
    }
    const validatePassword = (_: any, value: string) => {
        const password = form.getFieldValue('password');
        if (value && password && value !== password) {
            return Promise.reject(new Error('Пароли не совпадают!'));
        }
        return Promise.resolve();
    };
    return (
        <div className={classes.reg_container}>
            <div className={classes.reg_form_container}>
                <Form
                    form={form}
                    onFinish={onFinish}
                    name="normal_login"
                    className={classes.reg_form}
                    initialValues={{ remember: true }}
                >
                    <h1 className={classes.reg_form_title}>Создать учетную запись</h1>
                    <Form.Item
                        name="email"
                        rules={[{ required: true, message: 'Пожалуйста введите Email!' }]}
                    >
                        <Input placeholder="Email" />
                    </Form.Item>
                    <Form.Item
                        name="first_name"
                        rules={[{ required: true, message: 'Пожалуйста введите имя!' }]}
                    >
                        <Input placeholder="Имя" />
                    </Form.Item>
                    <Form.Item
                        name="last_name"
                        rules={[{ required: true, message: 'Пожалуйста введите фамилию!' }]}
                    >
                        <Input placeholder="Фамилия" />
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[{ required: true, message: 'Пожалуйста введите пароль!' }]}
                    >
                        <Input.Password
                            type="password"
                            placeholder="Пароль"
                        />
                    </Form.Item>
                    <Form.Item
                        name="password2"
                        rules={[{ required: true, message: 'Пожалуйста повторите пароль!' }, { validator: validatePassword }]}
                    >
                        <Input.Password
                            type="password"
                            placeholder="Повторите пароль"
                        />
                    </Form.Item>
                    <Form.Item
                        name="username"
                        rules={[{ required: true, message: 'Пожалуйста введите логин!' }]}
                    >
                        <Input
                            placeholder="Логин"
                        />
                    </Form.Item>
                    <Form.Item >
                        <Button loading={loading} type="primary" htmlType="submit">
                            Создать
                        </Button>
                        <Link to={'/login'} className={classes.reg_form_auth}>У вас уже есть учетная запись?</Link>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );
};

export default RegistrationPage;