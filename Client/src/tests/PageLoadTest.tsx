import React, { useEffect } from 'react';

interface Props {
    onLoaded: (loadTime: number) => void;
}

const PageLoadTest: React.FC<Props> = ({ onLoaded }) => {
    useEffect(() => {
        const startTime = performance.now(); 
        window.addEventListener('load', () => {
            const endTime = performance.now();
            const timeTaken = endTime - startTime;
            onLoaded(timeTaken);
        });
    }, [onLoaded]);

    return null;
};

export default PageLoadTest;