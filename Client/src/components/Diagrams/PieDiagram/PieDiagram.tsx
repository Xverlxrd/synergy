import React, {FC} from 'react';
import {Cell, Pie, PieChart} from "recharts";
import {PieProps} from "@/components/Diagrams/PieDiagram/type";
import {
    startOfDay,
    endOfDay,
    startOfWeek,
    endOfWeek,
    startOfMonth,
    endOfMonth,
    isWithinInterval,
    parseISO,
    startOfYear, endOfYear
} from 'date-fns';
import {TTask} from "@/helpers/types/projectType";

const PieDiagram: FC<PieProps> = ({ tasks, tab }) => {
    const now = new Date();


    const filterTasksByTab = (tasks: TTask[], tab: number) => {
        switch (tab) {
            case 1:
                return tasks?.filter(task => isWithinInterval(parseISO(task.created_at), { start: startOfDay(now), end: endOfDay(now) }));
            case 2:
                return tasks?.filter(task => isWithinInterval(parseISO(task.created_at), { start: startOfWeek(now), end: endOfWeek(now) }));
            case 3:
                return tasks?.filter(task => isWithinInterval(parseISO(task.created_at), { start: startOfMonth(now), end: endOfMonth(now) }));
            case 4:
                return tasks?.filter(task => isWithinInterval(parseISO(task.created_at), { start: startOfYear(now), end: endOfYear(now) }));
            default:
                return tasks;
        }
    };

    const filteredTasks = filterTasksByTab(tasks, tab);
    const completedTasks = filteredTasks?.filter(task => task.completed);
    const notCompletedTasks = filteredTasks?.filter(task => !task.completed);
    
    const data = [
        { name: 'Выполненные', value: completedTasks?.length },
        { name: 'Невыполненные', value: notCompletedTasks?.length },
    ];

    const COLORS = ['#2ED47A', '#FF8042'];

    const RADIAN = Math.PI / 180;
    // @ts-ignore
    const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
        const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
        const x = cx + radius * Math.cos(-midAngle * RADIAN);
        const y = cy + radius * Math.sin(-midAngle * RADIAN);

        return (
            <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
                {`${(percent * 100).toFixed(0)}%`}
            </text>
        );
    };

    return (
        <PieChart width={250} height={300}>
            <Pie
                data={data}
                cx="50%"
                cy="50%"
                labelLine={false}
                label={renderCustomizedLabel}
                outerRadius={120}
                fill="#8884d8"
                dataKey="value"
            >
                {data.map((entry, index) => (
                    <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                ))}
            </Pie>
        </PieChart>
    );
};

export default PieDiagram;