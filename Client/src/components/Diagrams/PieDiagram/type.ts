import {TTask} from "@/helpers/types/projectType";

export interface PieProps {
    tasks: TTask[],
    tab: number
}