import React, { FC } from 'react';
import { Area, AreaChart, CartesianGrid, XAxis, YAxis, Tooltip } from "recharts";
import { AreaProps } from "@/components/Diagrams/AreaDiagram/type";
import moment from "moment";
import {
    startOfDay,
    endOfDay,
    startOfWeek,
    endOfWeek,
    startOfMonth,
    endOfMonth,
    isWithinInterval,
    eachDayOfInterval,
    eachWeekOfInterval,
    eachMonthOfInterval,
    format,
    parseISO,
    startOfYear, endOfYear
} from 'date-fns';
import {TTask} from "@/helpers/types/projectType";

const AreaDiagram: FC<AreaProps> = ({ tasks, tab }) => {
    const now = new Date();

    const filterTasksByTab = (tasks: TTask[], tab: number) => {
        switch (tab) {
            case 1:
                return tasks?.filter(task => isWithinInterval(parseISO(task.created_at), { start: startOfDay(now), end: endOfDay(now) }));
            case 2:
                return tasks?.filter(task => isWithinInterval(parseISO(task.created_at), { start: startOfWeek(now), end: endOfWeek(now) }));
            case 3:
                return tasks?.filter(task => isWithinInterval(parseISO(task.created_at), { start: startOfMonth(now), end: endOfMonth(now) }));
            case 4:
                return tasks?.filter(task => isWithinInterval(parseISO(task.created_at), { start: startOfYear(now), end: endOfYear(now) }));
            default:
                return tasks;
        }
    };

    const getIntervalByTab = (tab: number) => {
        switch (tab) {
            case 1:
                return eachDayOfInterval({ start: startOfDay(now), end: endOfDay(now) });
            case 2:
                return eachDayOfInterval({ start: startOfWeek(now), end: endOfWeek(now) });
            case 3:
                return eachDayOfInterval({ start: startOfMonth(now), end: endOfMonth(now) });
            case 4:
                return eachDayOfInterval({ start: startOfYear(now), end: endOfYear(now) });
            default:
                return [];
        }
    };

    const filteredTasks = filterTasksByTab(tasks, tab);
    const intervals = getIntervalByTab(tab);

    const data = intervals.map(date => {
        const formattedDate = format(date, 'dd.MM.yyyy');
        const completedTasksCount = filteredTasks?.filter(task => format(parseISO(task.created_at), 'dd.MM.yyyy') === formattedDate && task.completed).length;
        return { date: formattedDate, completed: completedTasksCount };
    });

    return (
        <AreaChart
            width={440}
            height={350}
            data={data}
            margin={{
                top: 10,
                right: 30,
                left: 0,
                bottom: 0,
            }}
        >
            <CartesianGrid strokeDasharray="1 1" />
            <XAxis dataKey="date" />
            <YAxis />
            <Tooltip />
            <Area type="monotone" dataKey="completed" stroke="#109CF1" fill="#C7EAFF" />
        </AreaChart>
    );
};

export default AreaDiagram;
