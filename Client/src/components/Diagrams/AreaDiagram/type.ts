import {TTask} from "@/helpers/types/projectType";

export interface AreaProps {
    tasks: TTask[],
    tab: number
}