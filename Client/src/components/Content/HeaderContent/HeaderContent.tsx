import React, {FC} from 'react';
import classes from './style.module.scss'
import {BellOutlined} from "@ant-design/icons";
import {Input} from "antd";
import {HeaderProps} from "@/components/Content/HeaderContent/type";

const HeaderContent:FC<HeaderProps> = ({setOpenNotifications, openNotifications}) => {
    return (
        <header className={classes.header}>
            <div></div>
            <BellOutlined
                onClick={() => setOpenNotifications(!openNotifications)}
                className={classes.header_notif}
            />
        </header>
    );
};

export default HeaderContent;