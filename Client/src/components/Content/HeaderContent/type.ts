import {Dispatch, SetStateAction} from "react";

export interface HeaderProps {
    setOpenNotifications: Dispatch<SetStateAction<boolean>>
    openNotifications: boolean
}