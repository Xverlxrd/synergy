import React, { useEffect, useState } from 'react';
import classes from './style.module.scss';
import { useSelector } from "react-redux";
import { RootState } from "@/app/store/reducers/rootReducer";
import { Table, TableColumnsType, Button } from "antd";
import { AuthService } from "@/helpers/utils/authService";
import { TUser } from "@/helpers/types/authTypes";
import { ProjectService } from "@/helpers/utils/projectService";
import { TTask } from "@/helpers/types/projectType";
import * as XLSX from 'xlsx';

interface DataType {
    key: React.Key;
    name: string;
    completed: number;
    overdue: number;
    performance: number;
}

const columns: TableColumnsType<DataType> = [
    {
        title: 'Имя',
        dataIndex: 'name',
    },
    {
        title: 'Количество завершенных задач',
        dataIndex: 'completed',
        sorter: {
            compare: (a, b) => a.completed - b.completed,
            multiple: 3,
        },
    },
    {
        title: 'Количество просроченных задач',
        dataIndex: 'overdue',
        sorter: {
            compare: (a, b) => a.overdue - b.overdue,
            multiple: 2,
        },
    },
    {
        title: 'Процент производительности | %',
        dataIndex: 'performance',
        sorter: {
            compare: (a, b) => a.performance - b.performance,
            multiple: 1,
        },
    },
];

const AnalizContent = () => {
    const project = useSelector((state: RootState) => state.project.project);
    const [users, setUsers] = useState<TUser[] | null>(null);
    const [tasks, setTasks] = useState<TTask[] | null>(null);

    const completedTask = tasks?.filter(task => task.completed === true).length || 0;
    const notCompletedTask = tasks?.filter(task => task.completed === false).length || 0;
    const projectProcent = tasks ? Math.round((completedTask / tasks.length) * 100) : 0;

    useEffect(() => {
        ProjectService.getTask(project.id)
            .then(res => {
                setTasks(res.data);
            });

        AuthService.allUsers()
            .then(res => {
                const projectMembers = project.members.concat(project.created_by);
                setUsers(res.data.filter(user => projectMembers.includes(user.id)));
            });
    }, [project.id]);

    const calculateUserStats = (user: TUser): DataType => {
        const userTasks = tasks?.filter(task => task.assigned_to === user.id) || [];
        const completedTasks = userTasks.filter(task => task.completed).length;
        const overdueTasks = 0; // здесь можно добавить логику для подсчета просроченных задач
        const totalTasks = userTasks.length;
        const productivity = totalTasks === 0 ? 0 : (completedTasks / totalTasks) * 100;

        return {
            key: String(user.id),
            name: user.first_name,
            completed: completedTasks,
            overdue: overdueTasks,
            performance: productivity,
        };
    };

    const [data, setData] = useState<DataType[]>([]);

    useEffect(() => {
        if (users) {
            const userData = users.map(user => calculateUserStats(user));
            setData(userData);
        }
    }, [users, tasks]);

    const exportToExcel = () => {
        const projectData = [
            { A: 'Название проекта', B: project.name },
            { A: 'Описание проекта', B: project.description },
            { A: 'Процент завершенности проекта', B: `${projectProcent}%` },
            { A: 'Количество выполненных задач', B: completedTask },
            { A: 'Количество невыполненных задач', B: notCompletedTask },
            { A: 'Время выполнения проекта', B: '47 дней и 18 часов' },
            { A: 'Время до дедлайна проекта', B: '20 дней и 12 часов' }
        ];

        const projectWorksheet = XLSX.utils.json_to_sheet(projectData, { skipHeader: true });

        const userWorksheet = XLSX.utils.json_to_sheet(data);

        const workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook, projectWorksheet, "Project Data");
        XLSX.utils.book_append_sheet(workbook, userWorksheet, "Users Data");

        XLSX.writeFile(workbook, "project_and_users_data.xlsx");
    };

    return (
        <div className={classes.analiz_container}>
            {users != null && (
                <>
                    <div className={classes.analiz_project}>
                        <h1 className={classes.analiz_project_title}>
                            Проект
                        </h1>
                        <ul className={classes.analiz_project_list}>
                            <li className={classes.analiz_project_item}>
                                <h3 className={classes.analiz_project_text}>Название проекта: {project.name}</h3>
                            </li>
                            <li className={classes.analiz_project_item}>
                                <h3 className={classes.analiz_project_text}>Описание проекта: {project.description}</h3>
                            </li>
                            <li className={classes.analiz_project_item}>
                                <h3 className={classes.analiz_project_text}>Процент завершенности проекта: {projectProcent}%</h3>
                            </li>
                            <li className={classes.analiz_project_item}>
                                <h3 className={classes.analiz_project_text}>Количество выполненных задач: {completedTask}</h3>
                            </li>
                            <li className={classes.analiz_project_item}>
                                <h3 className={classes.analiz_project_text}>Количество невыполненных задач: {notCompletedTask}</h3>
                            </li>
                            <li className={classes.analiz_project_item}>
                                <h3 className={classes.analiz_project_text}>Время выполнения проекта: 47 дней и 18 часов </h3>
                            </li>
                            <li className={classes.analiz_project_item}>
                                <h3 className={classes.analiz_project_text}>Время до дедлайна проекта: 20 дней и 12 часов</h3>
                            </li>
                        </ul>
                    </div>
                    <div className={classes.analiz_users}>
                        <h1 className={classes.analiz_project_title}>
                            Пользователи
                        </h1>
                        <Table columns={columns} dataSource={data} />
                    </div>
                </>
            )}
            <Button onClick={exportToExcel} type="primary" style={{ marginBottom: 16 }}>
                Экспортировать в Excel
            </Button>
        </div>
    );
};

export default AnalizContent;
