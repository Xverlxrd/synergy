import React, {FC, useEffect, useState} from 'react';
import {Button, message, Modal, Popconfirm, PopconfirmProps, Select} from "antd";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@/app/store/reducers/rootReducer";
import { AuthService } from "@/helpers/utils/authService";
import { ProjectService } from "@/helpers/utils/projectService";
import { setProject } from "@/app/store/actions/projectActions";
import { NotificationService } from "@/helpers/utils/notificationService";
import { useNavigate } from "react-router-dom";
import classes from './style.module.scss';
import {TUser} from "@/helpers/types/authTypes";
import {setNavbarRefresh} from "@/app/store/actions/navbarActions";
import {UserDeleteOutlined} from "@ant-design/icons";
import AnalizContent from "@/components/Content/SettingsContent/ProjectContent/AnalizContent/AnalizContent";

const { Option } = Select;

const ProjectContent = () => {
    const [users, setUsers] = useState(null);
    const [friends, setFriends] = useState(null);
    const [userModal, setUserModal] = useState(false);
    const [selectedUsers, setSelectedUsers] = useState([]);
    const [members, setMembers] = useState([]);
    const [openAnaliz, setOpenAnaliz] = useState(false)

    const navigate = useNavigate();
    const dispatch = useDispatch();

    const project = useSelector((state: RootState) => state.project.project);
    const user = useSelector((state: RootState) => state.user.user);
    const navbarRefresh = useSelector((state: RootState) => state.navbarRefresh.navbarRefresh)

    const confirm: PopconfirmProps['onConfirm'] = (e) => {
        ProjectService.updateProject(project.id, project.members, true)
            .then(res => {
                dispatch(setProject(res.data))
            })
    };

    const handleOk = () => {
        setOpenAnaliz(false);
    };

    const handleCancel = () => {
        setOpenAnaliz(false);
    };

    const cancel: PopconfirmProps['onCancel'] = (e) => {
    };


    useEffect(() => {
        AuthService.allUsers()
            .then(response => {
                setUsers(response.data);
                setFriends(response.data.filter(juser =>
                    user?.contacts.includes(juser.id)
                ));
                setMembers(response.data.filter(juser =>
                    project?.members.includes(juser.id)
                ));
            })
            .catch(() => {
                message.error("Ой! Что-то пошло не так!!!");
            });
    }, [user, project]);

    useEffect(() => {
        if (userModal && members) {
            setSelectedUsers(members.map(member => String(member.id)));
        }
    }, [userModal, members]);

    const projectInfo = () => {
        ProjectService.projectInfo(project?.id)
            .then(response => {
                dispatch(setProject(response.data));
            });
    };

    const addUserToProject = () => {
        const data = {
            message: `Вас пригласили в проект "${project?.name}"`,
            users: selectedUsers,
            project: project?.id,
            members: project?.members,
            created_by: user?.id
        };
        NotificationService.createNotification(data)
            .then((response) => {
                projectInfo()
                message.success(`Вы пригласили пользователя в проект!`)
            })
            .catch(() => {
                message.error('Этого пользователя не получилось добавить!');
            });
        setUserModal(false);
    };

    const notificationToDeleteUser = (delete_user_id:number) => {
        const data = {
            message: `Вас удалили из проекта "${project?.name}"`,
            users: [delete_user_id],
        };
        NotificationService.createNotification(data)
            .then((response) => {
                projectInfo()
                message.success(`Вы удалили пользователя из проекта!`)
            })
            .catch(() => {
                message.error('Не получилось удалить пользователя!');
            });
    }

    const deleteUserInProject = (delete_user_id: number) => {
        const filtredMembers = members.filter(member => member.id !== delete_user_id)
        ProjectService.updateProject(project?.id, filtredMembers?.map(member => member.id))
            .then(response => {
                projectInfo()
                notificationToDeleteUser(delete_user_id)
            })
            .catch(() => {

            })
    }

    const handleChangeSelect = (values: string[]) => {
        setSelectedUsers(values.map(Number));
    };

    const exitProject = () => {
        const filtredMembers = members.filter(member => member.id !== user?.id);
            ProjectService.updateProject(project?.id, filtredMembers?.map(member => member.id))
                .then(() => {
                    message.success('Вы успешно вышли из проекта!');
                    dispatch(setNavbarRefresh(!navbarRefresh))
                    navigate('/home');
                })
                .catch(() => {
                    message.error('Не удалось покинуть проект!');
                });
    };

    return (
        <div className={classes.content}>
            <Modal
                className={classes.modal}
                open={userModal}
                onCancel={() => setUserModal(false)}
                onOk={addUserToProject}
            >
                <Select
                    className={classes.modal_members}
                    mode="multiple"
                    placeholder="Выберите из списка контактов"
                    onChange={handleChangeSelect}
                    value={selectedUsers.map(String)}
                >
                    {friends && friends.map((friend: TUser) => (
                        <Option key={friend.id.toString()} value={friend.id.toString()}>
                            {friend.first_name} {friend.last_name}
                        </Option>
                    ))}
                </Select>
            </Modal>
            <Modal
                width={800}
                title="Статистика"
                open={openAnaliz}
                footer={[
                    <Button key="save" type="primary" onClick={handleOk}>
                        Закрыть
                    </Button>,
                ]}
                onCancel={handleCancel}
            >
                <AnalizContent/>
            </Modal>
            <header className={classes.content_header}>
                <h1 className={classes.content_header_name}>{project?.name}</h1>
                {project?.created_by === user?.id ? (
                    <div className={classes.content_header_btns}>
                        {!project.isEnd ? (
                            <Popconfirm
                                title="Завершить проект"
                                description="Вы действительно хотите завершить проект?"
                                onConfirm={confirm}
                                onCancel={cancel}
                                okText="Да"
                                cancelText="Нет"
                            >
                                <Button className={classes.content_header_end}>Завершить</Button>
                            </Popconfirm>
                        ) : (
                            <>
                                <Button className={classes.content_header_delete}>Удалить</Button>
                                <Button
                                    onClick={() => setOpenAnaliz(true)}
                                    className={classes.content_header_analiz}
                                >
                                    Статистика
                                </Button>
                            </>
                        )}
                    </div>
                ) : (
                    <Popconfirm
                        placement="left"
                        title='Выйти из проекта'
                        description='Вы уверены что хотите покинуть проект?'
                        onConfirm={exitProject}
                        okText="Да"
                        cancelText="Нет"
                    >
                        <Button className={classes.content_header_btn}>Выйти</Button>
                    </Popconfirm>
                )}
            </header>

            <div className={classes.content_members}>
                <h1 className={classes.content_members_title}>Участники</h1>
                {project?.created_by === user?.id && (
                    <Button
                        className={classes.content_members_btn}
                        onClick={() => setUserModal(true)}
                    >
                        Добавить пользователя
                    </Button>

                )}
                <div className={classes.content_members_list}>
                    {members?.map(member => (
                        <div key={member.id} className={classes.content_members_item}>
                            <div className={classes.content_members_info}>
                                <img
                                    className={classes.content_members_img}
                                    src={member.image}
                                    alt='Avatar'
                                />
                                <p
                                    className={classes.content_members_name}
                                >
                                    {member.first_name} {member.last_name}
                                </p>
                            </div>
                            {user?.id === project?.created_by && (
                                <UserDeleteOutlined
                                    onClick={() => deleteUserInProject(member?.id)}
                                    className={classes.content_members_remove}
                                />
                            )}
                        </div>
                    ))}
                </div>
            </div>

            <div className={classes.content_date}>
                <div className={classes.content_date_time}>
                    <h1 className={classes.content_date_title}>Дата создания</h1>
                    <p className={classes.content_date_text}>{project?.start_date}</p>
                </div>
                <div className={classes.content_date_time}>
                    <h1 className={classes.content_date_title}>Дата окончания</h1>
                    <p className={classes.content_date_text}>{project?.end_date}</p>
                </div>
            </div>

            <div className={classes.content_description}>
                <h1 className={classes.content_description_title}>Описание</h1>
                <p className={classes.content_description_text}>{project?.description}</p>
            </div>
        </div>
    );
};

export default ProjectContent;
