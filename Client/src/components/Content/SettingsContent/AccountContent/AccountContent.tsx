import React, {useState} from 'react';
import classes from './style.module.scss'
import {Button, Form, Input, message, Modal} from "antd";
import FormItem from "antd/es/form/FormItem";
import {AuthService} from "@/helpers/utils/authService";
import {useDispatch, useSelector} from "react-redux";
import {setUser} from "@/app/store/actions/userActions";
import {RootState} from "@/app/store/reducers/rootReducer";
const AccountContent = () => {
    const user = useSelector((state: RootState) => state.user.user);
    const dispatch = useDispatch()
    const [form] = Form.useForm();
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [whoseIsModal, setWhoseIsModal] = useState<null | string>(null)

    const openModal = (modalName: string) => {
        let whoseIsModalValue = '';

        switch (modalName) {
            case 'first_name':
                whoseIsModalValue = 'first_name';
                break;
            case 'last_name':
                whoseIsModalValue = 'last_name';
                break;
            case 'email':
                whoseIsModalValue = 'email';
                break;
            case 'password':
                whoseIsModalValue = 'password';
                break;
            case 'username':
                whoseIsModalValue = 'username';
                break;
            default:
                whoseIsModalValue = null;
        }

        setWhoseIsModal(whoseIsModalValue);
        setIsModalOpen(true);
    };
    const handleOk = () => {
        const formData = form.getFieldsValue();

        AuthService.update(formData)
            .then(response => {
                setIsModalOpen(false);
                setWhoseIsModal(null)
                message.success('Данные успешно изменены')
                dispatch(setUser(response.data))
            })
            .catch(() => {
                message.error("Упс! Произошла ошибка!")
            })



    };

    const handleCancel = () => {
        setIsModalOpen(false);
        setWhoseIsModal(null)
    };

    const modalContent = () => {
        switch (whoseIsModal) {
            case 'first_name':
                return (
                    <FormItem label='Новое имя' name='first_name'>
                        <Input/>
                    </FormItem>
                )
            case 'last_name':
                return (
                    <FormItem label='Новая фамилия' name='last_name'>
                        <Input/>
                    </FormItem>
                )
            case 'email':
                return (
                    <FormItem label='Новый email' name='email'>
                        <Input/>
                    </FormItem>
                )
            case 'password':
                return (
                    <FormItem label='Новый пароль' name='password'>
                        <Input.Password/>
                    </FormItem>
                )
            case 'username':
                return (
                    <FormItem label='Новый логин' name='username'>
                        <Input/>
                    </FormItem>
                )
        }
    }

    return (
        <div className={classes.content}>
            <Modal
                title="Изменить имя"
                open={isModalOpen}
                onCancel={handleCancel}
                onOk={handleOk}
                okText="Сохранить"
                cancelText="Отмена"
            >
                <Form
                    form={form}
                    layout='vertical'
                >
                    {modalContent()}
                    {/*<FormItem label='Пароль' name='password'>*/}
                    {/*    <Input.Password/>*/}
                    {/*</FormItem>*/}
                </Form>
            </Modal>
            <div className={classes.content_header}>
                <div className={classes.content_header_userdata}>
                    <div className={classes.content_header_img}>
                        <img className={classes.img} src={user?.image} alt='Нет аватара'/>
                    </div>
                    <div className={classes.content_header_info}>
                        <h1 className={classes.content_header_name}>{user?.first_name} {user?.last_name}</h1>
                        <p className={classes.content_header_email}>{user?.email}</p>
                    </div>
                </div>
            </div>
            <ul className={classes.content_account}>
                <li className={classes.content_account_item}>
                    <div className={classes.content_account_text}>
                        <span className={classes.content_account_label}>Имя</span>
                        <p className={classes.content_account_info}>{user?.first_name}</p>
                    </div>
                    <Button
                        onClick={() => openModal('first_name')}
                        className={classes.content_account_btn}
                        type='default'
                    >
                        Редактировать
                    </Button>
                </li>
                <li className={classes.content_account_item}>
                    <div className={classes.content_account_text}>
                        <span className={classes.content_account_label}>Фамилия</span>
                        <p className={classes.content_account_info}>{user?.last_name}</p>
                    </div>

                    <Button
                        onClick={() => openModal('last_name')}
                        className={classes.content_account_btn}
                        type='default'
                    >
                        Редактировать
                    </Button>
                </li>
                <li className={classes.content_account_item}>
                    <div className={classes.content_account_text}>
                        <span className={classes.content_account_label}>Email</span>
                        <p className={classes.content_account_info}>{user?.email}</p>
                    </div>

                    <Button
                        onClick={() => openModal('email')}
                        className={classes.content_account_btn}
                        type='default'
                    >
                        Редактировать
                    </Button>
                </li>
                <li className={classes.content_account_item}>
                    <div className={classes.content_account_text}>
                        <span className={classes.content_account_label}>Пароль</span>
                        <p className={classes.content_account_info}>********</p>
                    </div>

                    <Button
                        onClick={() => openModal('password')}
                        className={classes.content_account_btn}
                        type='default'
                    >
                        Редактировать
                    </Button>
                </li>
                <li className={classes.content_account_item}>
                    <div className={classes.content_account_text}>
                        <span className={classes.content_account_label}>Логин</span>
                        <p className={classes.content_account_info}>{user?.username}</p>
                    </div>

                    <Button
                        onClick={() => openModal('username')}
                        className={classes.content_account_btn}
                        type='default'
                    >
                        Редактировать
                    </Button>
                </li>
            </ul>
        </div>
    );
};

export default AccountContent;