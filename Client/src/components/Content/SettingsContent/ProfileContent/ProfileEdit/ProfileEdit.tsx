import React from 'react';
import classes from "@/components/Content/SettingsContent/ProfileContent/style.module.scss";
import {Button, Form, Input, Upload} from "antd";
import {PlusOutlined} from "@ant-design/icons";
import {AuthService} from "@/helpers/utils/authService";
import {TUpdateData, TUser, TUserData} from "@/helpers/types/authTypes";
import {ProfileProps} from "@/components/Content/SettingsContent/ProfileContent/types";
import {useDispatch, useSelector} from "react-redux";
import {setUser} from "@/app/store/actions/userActions";
import {RootState} from "@/app/store/reducers/rootReducer";

const ProfileEdit:React.FC<ProfileProps> = ({setActiveMenu}) => {
    const user = useSelector((state: RootState) => state.user.user);
    const dispatch = useDispatch()
    const [form] = Form.useForm();

    const onFinish = (values: TUpdateData) => {
        const formData = new FormData();
        const filteredValues: TUpdateData = Object.fromEntries(
            Object.entries(values).filter(([key, value]) => value !== '' && value !== null)
        ) as TUpdateData;

        if (typeof filteredValues.image === 'string') {
            delete filteredValues.image;
        }
        for (const [key, value] of Object.entries(values)) {
            if (value !== null && value !== undefined) {
                if (typeof value === 'object' && 'file' in value) {
                    formData.append(key, value.file);
                } else {
                    formData.append(key, String(value));
                }
            }
        }

        AuthService.update(formData)
            .then(response => {
                dispatch(setUser(response.data))
                setActiveMenu('profile')
            })
            .catch(error => {

            });
    };

    return (
        <Form
            form={form}
            onFinish={onFinish}
            className={classes.form}
            initialValues={{
                skills: user?.skills,
                job_title: user?.job_title,
                work_experience: user?.work_experience,
                about: user?.about,
                image: null,
            }}
        >
            <div className={classes.content}>
                <div className={classes.content_header}>
                    <div className={classes.content_header_userdata}>
                        <Form.Item name='image' className={classes.form_item}>
                            <Upload
                                maxCount={1}
                                name='image'
                                action='http://127.0.0.1:8000/media/user_images/'
                                listType={"picture-circle"}
                                beforeUpload={() => false}
                            >
                                <Button className={classes.content_header_photo} >
                                    <PlusOutlined  className={classes.content_header_plus}/>
                                </Button>
                            </Upload>
                        </Form.Item>

                        <div className={classes.content_header_info}>
                            <h1 className={classes.content_header_name}>{user?.first_name} {user?.last_name}</h1>
                            <p className={classes.content_header_email}>{user?.email}</p>
                        </div>
                    </div>
                    <div className={classes.content_btn}>
                        <Button className='primary-blue' htmlType="submit">Сохранить</Button>
                    </div>
                </div>
                <div className={classes.content_resume}>
                    <div className={classes.content_resume_workinfo}>
                        <div className={classes.content_resume_jobtitle}>
                            <h1 className={classes.content_resume_title}>Желаемая должность</h1>
                            <Form.Item
                                className={classes.form_item}
                                name="job_title"
                            >
                                <Input type="text" className={classes.content_resume_text}/>
                            </Form.Item>
                        </div>
                        <div className={classes.content_resume_workexperience}>
                            <h1 className={classes.content_resume_title}>Стаж работы</h1>
                            <Form.Item
                                className={classes.form_item}
                                name="work_experience"
                            >
                                <Input type="text"  className={classes.content_resume_text}/>
                            </Form.Item>
                        </div>
                    </div>
                    <div className={classes.content_resume_skilsinfo}>
                        <div className={classes.content_resume_skils}>
                            <h1 className={classes.content_resume_title}>Навыки</h1>
                            <Form.Item
                                className={classes.form_item}
                                name="skills"
                            >
                                <Input.TextArea
                                    maxLength={250}
                                    autoSize={{ minRows: 1, maxRows: 2 }}
                                    className={classes.content_resume_text}
                                />
                            </Form.Item>
                        </div>
                    </div>
                    <div className={classes.content_resume_aboutinfo}>
                        <div className={classes.content_resume_about}>
                            <h1 className={classes.content_resume_title}>О себе</h1>
                            <Form.Item
                                className={classes.form_item}
                                name="about"
                            >
                                <Input.TextArea
                                    maxLength={250}
                                    autoSize={{ minRows: 1, maxRows: 3 }}
                                    className={classes.content_resume_text}
                                />
                            </Form.Item>
                        </div>
                    </div>
                </div>
            </div>
        </Form>
    );
};

export default ProfileEdit;