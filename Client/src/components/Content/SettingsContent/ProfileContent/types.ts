import React from "react";
import {TUser} from "@/helpers/types/authTypes";

export interface ProfileProps {
    setActiveMenu?: React.Dispatch<React.SetStateAction<string>>
    user: TUser
    addUser?: (user_id: number) => void
    deleteUser?: (user_id: number) => void
}
