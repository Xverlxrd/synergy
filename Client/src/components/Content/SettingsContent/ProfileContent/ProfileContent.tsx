import React from 'react';
import classes from "./style.module.scss";
import {Button} from "antd";
import {isNotAllUserData} from "@/components/Content/SettingsContent/ProfileContent/constans";
import {ProfileProps} from "@/components/Content/SettingsContent/ProfileContent/types";
import {useSelector} from "react-redux";
import {TUser, TUserData} from "@/helpers/types/authTypes";
import {RootState} from "@/app/store/reducers/rootReducer";

const ProfileContent: React.FC<ProfileProps> = ({setActiveMenu, deleteUser, user, addUser}) => {
    const currentUser = useSelector((state: RootState) => state.user.user);

    return (
        <div className={classes.content}>
            <div className={classes.content_header}>
                <div className={classes.content_header_userdata}>
                    <div className={classes.content_header_img}>
                        <img className={classes.img} src={user?.image} alt='Нет аватара'/>
                    </div>
                    <div className={classes.content_header_info}>
                        <h1 className={classes.content_header_name}>{user?.first_name} {user?.last_name}</h1>
                        <p className={classes.content_header_email}>{user?.email}</p>
                    </div>
                </div>
                <div className={classes.content_btn}>
                    {addUser !== undefined ? (
                        <Button
                            className={currentUser?.contacts.includes(user?.id)
                                ? classes.content_btn_delete
                                : classes.content_btn_add}
                            onClick={() => currentUser?.contacts.includes(user?.id) ? deleteUser(user?.id) : addUser(user?.id)}
                        >
                            {currentUser?.contacts.includes(user?.id)
                                ? 'Удалить'
                                : 'Добавить'
                            }
                        </Button>
                    ) : (
                        <Button
                            className='primary-blue'
                            onClick={() => setActiveMenu('profile_edit')}
                        >
                            Изменить
                        </Button>
                    )}
                </div>
            </div>
            <div className={classes.content_resume}>
                <div className={classes.content_resume_workinfo}>
                    <div className={classes.content_resume_jobtitle}>
                        <h1 className={classes.content_resume_title}>Желаемая должность</h1>
                        <p className={classes.content_resume_text}>{user?.job_title || isNotAllUserData}</p>
                    </div>
                    <div className={classes.content_resume_workexperience}>
                        <h1 className={classes.content_resume_title}>Стаж работы</h1>
                        <p className={classes.content_resume_text}>{user?.work_experience || isNotAllUserData}</p>
                    </div>
                </div>
                <div className={classes.content_resume_skilsinfo}>
                    <div className={classes.content_resume_skils}>
                        <h1 className={classes.content_resume_title}>Навыки</h1>
                        <p className={classes.content_resume_text}>
                            {user?.skills || isNotAllUserData}
                        </p>
                    </div>
                </div>
                <div className={classes.content_resume_aboutinfo}>
                    <div className={classes.content_resume_about}>
                        <h1 className={classes.content_resume_title}>О себе</h1>
                        <p className={classes.content_resume_text}>{user?.about || isNotAllUserData}</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProfileContent;