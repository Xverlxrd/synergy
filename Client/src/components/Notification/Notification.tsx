import React, {useEffect, useState} from 'react';
import classes from './style.module.scss';
import {NotificationService} from "@/helpers/utils/notificationService";
import {Button, message} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "@/app/store/reducers/rootReducer";
import {TNotification} from "@/helpers/types/notificationType";
import {ProjectService} from "@/helpers/utils/projectService";
import {setNavbarRefresh} from "@/app/store/actions/navbarActions";
import {CloseOutlined} from "@ant-design/icons";

const Notification = () => {
    const [notifications, setNotifications] = useState<TNotification[] | null>([])
    const [destroyNotification, setDestroyNotification] = useState(false)

    const dispatch = useDispatch()

    const user = useSelector((state: RootState) => state.user.user);
    const navbarRefresh = useSelector((state:RootState) => state.navbarRefresh.navbarRefresh)

    const allNotif = () => {
        NotificationService.allNotifications()
            .then(response => {
                const filteredNotifications = response.data.filter(notification =>
                    notification.users.includes(user.id)
                );
                setNotifications(prevNotifications => [...prevNotifications, ...filteredNotifications]);
            })
            .catch(error => {
                message.error('Не удалось получить уведомления!');
            });
    }

    useEffect(() => {
        if (!user) message.error('Войдите в аккаунт или зарегистрируйтесь!');
        allNotif()

    }, [user, destroyNotification]);

    const deleteNotification = (notification_id: number) => {
        NotificationService.deleteNotification(notification_id)
            .then(response => {
                message.success('Уведомление удалено!');
                setNotifications(prevNotifications =>
                    prevNotifications.filter(notification => notification.id !== notification_id)
                );
                setDestroyNotification(!destroyNotification)
                allNotif()
            })
            .catch(() => {
                message.error('Не удалось удалить уведомление!');
            });
    }

    const acceptProject = (
        project_id: number,
        members: number[],
        notification_id: number,
        created_by: number
    ) => {
        const data = {
            message: 'Пользователь принял приглашение в проект',
            is_system: true,
            users: [created_by]
        }
        ProjectService.updateProject(project_id, [...members, user?.id])
            .then(response => {
                message.success('Вы вступили в новый проект!')
            })
            .catch(() => {
                message.error('Не удалось принять приглашение!')
            })

        NotificationService.createNotification(data)
            .then(() => {
                deleteNotification(notification_id)
                dispatch(setNavbarRefresh(!navbarRefresh))
            })
            .catch(() => {

            })
    }

    const rejectProject = (notification_id: number, created_by: number) => {
        const data = {
            message: 'Пользователь отклонил приглашение в проект',
            is_system: true,
            users: [created_by]
        }
        NotificationService.createNotification(data)
            .then(() => {
                deleteNotification(notification_id)
                dispatch(setNavbarRefresh(!navbarRefresh))
            })
            .catch(() => {

            })
    }

    return (
        <div className={classes.container}>
            <div className={classes.notifications}>
                {notifications?.length > 0 && notifications.map(notification => (
                    <div key={notification.id} className={classes.notification}>
                        <p className={classes.notification_message}>{notification.message}</p>
                        {notification?.project && (
                            <div className={classes.notification_btn}>
                                <Button
                                    onClick={() =>
                                        acceptProject(
                                            notification?.project,
                                            notification?.members,
                                            notification?.id,
                                            notification?.created_by
                                        )}
                                    className={classes.notification_btn_accept}
                                >
                                    Принять
                                </Button>
                                <Button
                                    onClick={() =>
                                        rejectProject(
                                            notification?.id,
                                            notification?.created_by
                                        )
                                }
                                    className={classes.notification_btn_reject}
                                >
                                    Отклонить
                                </Button>
                            </div>
                        )}
                        <CloseOutlined onClick={() => deleteNotification(notification?.id)}/>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Notification;