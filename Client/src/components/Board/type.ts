import {TColumn, TTask} from "@/helpers/types/projectType";
import {Dispatch, SetStateAction} from "react";

export interface BoardProps {
    columns: TColumn[] | null
    updateColumn: boolean
    setUpdateColumn:  Dispatch<SetStateAction<boolean>>
    tasks: TTask[] | null
    updateTasks: () => void
}