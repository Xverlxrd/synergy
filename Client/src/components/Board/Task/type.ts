import {TColumn, TTask} from "@/helpers/types/projectType";

export interface TaskProps {
    task: TTask | null
    column: TColumn | null
    updateTasks: () => void
    handleDragStart: () => void;
    handleDrop: () => void;
}