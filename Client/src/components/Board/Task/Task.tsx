import React, {FC, useEffect, useRef, useState} from 'react';
import classes from "./style.module.scss";
import {
    BarChartOutlined,
    CalendarOutlined,
    CheckCircleFilled,
    MoreOutlined
} from "@ant-design/icons";
import {TaskProps} from "@/components/Board/Task/type";
import {TTask} from "@/helpers/types/projectType";
import {DatePicker, Dropdown, Form, Input, MenuProps, message, Modal, Select} from "antd";
import {ProjectService} from "@/helpers/utils/projectService";
import FormItem from "antd/es/form/FormItem";
import moment from "moment";
import {TUser} from "@/helpers/types/authTypes";
import {useSelector} from "react-redux";
import {RootState} from "@/app/store/reducers/rootReducer";
import {Option} from "antd/lib/mentions";
import {AuthService} from "@/helpers/utils/authService";


const items: MenuProps['items'] = [
    {
        label: 'Удалить',
        key: '1',
    },
    {
        label: 'Изменить',
        key: '2',
    },
];
const Task: FC<TaskProps> = ({task, column, handleDrop, handleDragStart, updateTasks}) => {
    const [taskUpdateModal, setTaskUpdateModal] = useState(false)
    const [users, setUsers] = useState<TUser[] | null>(null)
    const [form] = Form.useForm()
    const currentDate = moment();
    const isDeadlinePassed = task?.due_date && moment(task.due_date).isBefore(currentDate, 'day');
    const project = useSelector((state: RootState) => state.project.project)
    const user = useSelector((state: RootState) => state.user.user)

    useEffect(() => {
        AuthService.allUsers()
            .then((response) => {
                const newUsers = response.data.filter(user => project?.members.includes(user.id));
                newUsers.push(user)
                setUsers(newUsers);
            })
            .catch(()=> {})
    }, []);
    const formatDueDate = (dueDate: string) => {
        return moment(dueDate).format('D MMM. YYYY [года]');
    };

    const priorityTask = (task: TTask) => {
        switch (task?.priority) {
            case 'low':
                return <BarChartOutlined className={classes.task_content_lowpriority}/>
            case 'medium':
                return <BarChartOutlined className={classes.task_content_mediumpriority}/>
            case 'high':
                return <BarChartOutlined className={classes.task_content_highpriority}/>
            default:
                return <BarChartOutlined className={classes.task_content_lowpriority}/>
        }
    }
    const handleMenuClick = (e: any, task_id: number) => {
        if (e.key === '1') {
            ProjectService.deleteTask(task_id)
                .then(response => {
                    message.success("Задача удалена")
                    updateTasks()
                })
                .catch(error => {
                    message.error(error.response)
                })
        } else if (e.key === '2') {
            setTaskUpdateModal(true)
        }
    };

    const completeTask = (complete: boolean, task_id:number) => {
        if (task_id) {
            const formData = {
                ...form.getFieldsValue(),
                completed: complete
            }

            ProjectService.updateTask(task_id, formData)
                .then(response => {
                    updateTasks()
                })
                .catch(error => {
                    message.error(error.response)
                })
        }
    }

    const updateTask = (task_id: number) => {
        if (task_id) {
            const formData = {
                ...form.getFieldsValue(),
                id: task_id
            };
            console.log(formData)

            if (formData.due_date) {
                const dateObject = formData.due_date.$d;
                const formattedDate = `${dateObject.getFullYear()}-${(dateObject.getMonth() + 1).toString().padStart(2, '0')}-${dateObject.getDate().toString().padStart(2, '0')}`;
                formData.due_date = formattedDate;
            }

            ProjectService.updateTask(task_id, formData)
                .then(response => {
                    message.success("Задача изменена!")
                    updateTasks()
                    setTaskUpdateModal(false)
                })
                .catch(error => {
                    message.error(error.response)
                    setTaskUpdateModal(false)
                })
        }
    }


    return (
        <div
            onDragStart={handleDragStart}
            onDragEnd={handleDrop}
            draggable
            key={task?.id}
            className={classes.task}>
            <Modal
                open={taskUpdateModal}
                onCancel={() => setTaskUpdateModal(false)}
                onOk={() => updateTask(task?.id)}
            >
                <Form
                    layout="vertical"
                    form={form}
                >
                    <FormItem name='title' label='Название'>
                        <Input/>
                    </FormItem>
                    <FormItem name='assigned_to' label='Отвественный'>
                        <Select>
                            {users?.map(user => (
                                <Option key={user.id.toString()} value={user.id.toString()}>
                                    <div className={classes.task_asign}>
                                        <img className={classes.task_asign_img} src={user?.image} alt='Аватарка'/>
                                        {user.first_name} {user.last_name}
                                    </div>
                                </Option>
                            ))}
                        </Select>
                    </FormItem>
                    <FormItem name='due_date' label='Дедлайн'>
                        <DatePicker placeholder='Выберите дату'/>
                    </FormItem>
                    <FormItem name='priority' label='Приоритет'>
                        <Select options={[
                            {
                                label: 'Низкий',
                                value: 'low'
                            },
                            {
                                label: 'Средний',
                                value: 'medium'
                            },
                            {
                                label: 'Высокий',
                                value: 'high'
                            },
                        ]}/>
                    </FormItem>
                </Form>
            </Modal>
            <div className={classes.task_header}>
                <div className={classes.task_header_content}>
                    {task?.completed
                        ?
                        <CheckCircleFilled
                            className={classes.task_header_complete_completed}
                            onClick={() => completeTask(false, task?.id)}
                        />
                        :
                        <CheckCircleFilled
                            className={classes.task_header_complete}
                            onClick={() => completeTask(true, task?.id)}
                        />
                    }
                    <p className={classes.task_header_title}>{task?.title}</p>
                </div>
                <Dropdown menu={{items, onClick: (e) => handleMenuClick(e, task?.id)}}>
                    <a onClick={(e) => e.preventDefault()}>
                        <MoreOutlined className={classes.more}/>
                    </a>
                </Dropdown>
            </div>
            <div className={classes.task_content}>
                {task?.priority && priorityTask(task)}
                {task?.due_date && (
                    <div
                        className={isDeadlinePassed ?
                            classes.task_content_deadline_end
                            :
                            classes.task_content_deadline}
                    >
                        <CalendarOutlined className={isDeadlinePassed ?
                            classes.task_content_dateicon_end
                            :
                            classes.task_content_dateicon}
                        />
                        <p className={classes.task_content_date}>{task?.due_date.toString()}</p>
                    </div>
                )}
                {task?.assigned_to && (
                    <img
                        src={users?.find(user =>
                            user.id === task.assigned_to
                        )?.image}
                        alt='Аватарка ответственного'
                        className={classes.task_content_assigned}
                    />
                )}
            </div>
        </div>
    );
};

export default Task;