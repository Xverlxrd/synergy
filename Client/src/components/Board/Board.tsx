import React, {useRef, useState} from 'react';
import classes from "./style.module.scss";
import {Button, Dropdown, Form, Input, MenuProps, message, Modal} from "antd";
import {
    MoreOutlined,
} from "@ant-design/icons";
import FormItem from "antd/es/form/FormItem";
import {BoardProps} from "@/components/Board/type";
import {TColumn, TTask, TTaskData} from "@/helpers/types/projectType";
import {ProjectService} from "@/helpers/utils/projectService";
import {useSelector} from "react-redux";
import {RootState} from "@/app/store/reducers/rootReducer";
import Task from "@/components/Board/Task/Task";
import moment from "moment";


const items: MenuProps['items'] = [
    {
        label: 'Удалить',
        key: '1',
    },
    {
        label: 'Изменить',
        key: '2',
    },
];

const options = [
    {value: 'low', label: 'Не важно'},
    {value: 'medium', label: 'Средне'},
    {value: 'high', label: 'Важно'},
]

const Board: React.FC<BoardProps> = ({columns, updateColumn, updateTasks, setUpdateColumn, tasks}) => {
    const project = useSelector((state: RootState) => state.project.project)
    const [newColumn, setNewColumn] = useState(false)
    const [form] = Form.useForm()
    const [taskModal, setTaskModal] = useState(false)
    const [currentColumnId, setCurrentColumnId] = useState<number | null>(null);
    const [dragColumnId, setDragColumnId] = useState<number | null>(null);
    const draggedTask = useRef<number | null>(null);
    const [showAll, setShowAll] = useState(false);


    const handleShowAll = () => {
        setShowAll(!showAll);
    };
    const handleDragStart = (task_id: number) => () => {
        draggedTask.current = task_id;
    };

    const handleDragOver = (e: React.DragEvent<HTMLDivElement>, column_id: number) => {
        e.preventDefault();
        setDragColumnId(column_id)
    };
    const handleDrop = ()  => {
        if (draggedTask.current !== null && dragColumnId !== null) {
            const data: TTask = {
                column: dragColumnId
            }

            ProjectService.updateTask(draggedTask.current, data)
                .then(response => {
                    updateTasks();
                })
        }
    };

    //Сортировка задач по дате создания
    const sortedTasks = tasks?.slice().sort((a, b) => {
        if (a?.created_at && b?.created_at) {
            return moment(a.created_at).isBefore(b.created_at) ? 1 : -1;
        }
        return 0;
    });

    const addTask = (column_id: number) => {
        setTaskModal(true)
        setCurrentColumnId(column_id)
    }

    const createTask = () => {
        if (currentColumnId !== null) {
            const formData = {
                ...form.getFieldsValue(),
                project: project?.id,
                column: currentColumnId
            };
            ProjectService.createTask(formData)
                .then(response => {
                    setTaskModal(false)
                    updateTasks()
                    message.success('Задача успешно создана!')
                })
                .catch(error => {
                    setTaskModal(false)
                    message.error(`Произошла ошибка ${error}`)
                })
        }
    }

    const createColumn = () => {
        const formData = {
            ...form.getFieldsValue(),
            project: project?.id
        };

        ProjectService.createColumn(formData, project?.id)
            .then((response) => {
                setUpdateColumn(!updateColumn)
            })
            .catch(error => {
                message.error('Ошибка создания')
            })
    }
    const handleMenuClick = (e: any, column_id: number) => {
        if (e.key === '1') {
            ProjectService.deleteColumns(column_id)
                .then(response => {
                    message.success("Колонка удалена")
                    setUpdateColumn(!updateColumn)
                })
                .catch( () => {
                    message.warning('Вы не можете удалить эту колонку')
                })
        }
    };

    return (
        <div className={classes.board_content}>
            {columns?.map((column: TColumn) => (
                <div className={classes.board_content_column} key={column?.id}
                     onDragOver={(e) => handleDragOver(e, column.id)}
                >
                    <div
                        className={classes.board_content_columnheader}
                    >
                        <h1 className={classes.board_content_columname}>{column?.name}</h1>
                        <Dropdown menu={{ items, onClick: (e) => handleMenuClick(e, column?.id) }}>
                            <a onClick={(e) => e.preventDefault()}>
                                <MoreOutlined className={classes.more} />
                            </a>
                        </Dropdown>
                    </div>
                    <p className={classes.board_content_addtask} onClick={() => addTask(column?.id)}>+ Добавить задачу</p>
                    <Modal
                        open={taskModal}
                        onOk={createTask}
                        onCancel={() => setTaskModal(false)}
                    >
                        <Form layout='vertical' form={form}>
                            <FormItem label='Название' name='title'>
                                <Input />
                            </FormItem>
                        </Form>
                    </Modal>
                    <div className={classes.board_content_tasks}>
                        {sortedTasks?.filter((task) => task?.column === column?.id)
                            .slice(0, showAll ?
                                sortedTasks?.filter(
                                    (task) => task?.column === column?.id).length : 3)
                            .map((task) => (
                                <Task
                                    key={task?.id}
                                    task={task}
                                    column={column}
                                    handleDragStart={handleDragStart(task.id)}
                                    handleDrop={handleDrop}
                                    updateTasks={updateTasks}
                                />
                            ))}
                        {!showAll && sortedTasks?.filter((task) => task?.column === column?.id).length > 3 && (
                            <p onClick={handleShowAll} className={classes.show_all_button}>
                                Показать все задачи (
                                {sortedTasks?.filter((task) => task?.column === column?.id).length}
                                )
                            </p>
                        )}
                        {showAll && sortedTasks?.filter((task) => task?.column === column?.id).length > 3 && (
                            <p onClick={handleShowAll} className={classes.hide_all_button}>
                                Скрыть архив задач
                            </p>
                        )}
                    </div>
                </div>
            ))}
            {/* Отображение формы для создания новой колонки */}
            {columns?.length < 3 && (
                <div className={classes.board_content_column_create}>
                    <h1 className={classes.board_content_columname}>Новая колонка</h1>
                    <p className={classes.board_content_addtask} onClick={() => setNewColumn(!newColumn)}>+ Создать
                        колонку</p>
                    {newColumn && (
                        <Form onFinish={createColumn} layout='vertical' form={form}>
                            <FormItem label='Введите название' name='name'>
                                <Input/>
                            </FormItem>
                            <Button type='primary' htmlType='submit'>Создать</Button>
                        </Form>
                    )}
                </div>
            )}
        </div>
    );
};

export default Board;