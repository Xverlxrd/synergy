import React, {useEffect, useState} from 'react';
import classes from "@/components/NavBar/style.module.scss";
import {
    BorderOutlined,
    ExpandAltOutlined,
    LineChartOutlined,
    MailOutlined, MessageOutlined, SettingOutlined,
    UsergroupAddOutlined
} from "@ant-design/icons";
import {Button, message, Select} from "antd";
import {Link, useNavigate} from "react-router-dom";
import {AuthService} from "@/helpers/utils/authService";
import noUser from '@/assets/user.png'
import {TProject} from "@/helpers/types/projectType";
import {useDispatch, useSelector} from "react-redux";
import {setUser} from "@/app/store/actions/userActions";
import {RootState} from "@/app/store/reducers/rootReducer";
import {ProjectService} from "@/helpers/utils/projectService";
import {setProject} from "@/app/store/actions/projectActions";

const NavBar = () => {
    const user = useSelector((state: RootState) => state.user.user);
    const project = useSelector((state: RootState) => state.project.project);
    const navbarRefresh = useSelector((state:RootState) => state.navbarRefresh.navbarRefresh)

    const dispatch = useDispatch()

    const navigate = useNavigate()

    const [projectsAsMember, setProjectsAsMember] = useState<TProject[] | null>(null)
    const [selectedProject, setSelectedProject] = useState<number | null>(null);

    const fetchProjectsAsMember = () => {
        ProjectService.allProjects()
            .then((response) => {
                const filteredProjects = response.data.filter(project =>
                    project?.members?.includes(user?.id)
                );
                setProjectsAsMember(filteredProjects);
                const userProjects = user?.projects || [];
                const allProjects = [...userProjects, ...filteredProjects];
                if (allProjects.length > 0) {
                    const firstProject = allProjects[0];
                    dispatch(setProject(firstProject));
                    setSelectedProject(firstProject.id);
                }
            })
            .catch(error => {
            });
    }

    useEffect(() => {
        fetchProjectsAsMember();
        if (user && user.projects && user.projects.length > 0) {
            const firstProject = user.projects[0];
            dispatch(setProject(firstProject));
            setSelectedProject(firstProject.id);
        } else {
            dispatch(setProject(null));
            setSelectedProject(null);
        }
    }, [user, navbarRefresh]);

    const logout = () => {
        AuthService.logout()
            .then(() => {
                navigate('/login')
                dispatch(setUser(null))
                message.success('Вы успешно вышли из аккаунта!')
            })
    }

    const selectProject = (value: number) => {
        ProjectService.projectInfo(value)
            .then(response => {
                dispatch(setProject(response.data))
                setSelectedProject(value);
                message.success('Вы выбрали проект ' + response.data.name)
            })
            .catch(error => {
                message.error('Ошибка ' + error)
            })
    }

    return (
        <div className={classes.navbar}>
            <div className={classes.navbar_header}>
                <Link to='/'>
                    <p className={classes.navbar_name}>Synergy</p>
                </Link>
                <ExpandAltOutlined className={classes.navbar_collapse}/>
            </div>
            <div className={classes.navbar_profile}>
                <Link to='/settings'>
                    <div className={classes.navbar_avatar}>
                        <img
                            className={classes.navbar_avatar_img}
                            src={user?.image ? user?.image : noUser}
                            alt={'Нет аватара'}
                        />
                    </div>
                </Link>
                {user ? (
                    <div className={classes.navbar_btn}>
                        <Button className={'primary-blue'} onClick={logout} >
                            Выйти
                        </Button>
                    </div>
                ) : (
                    <Link className={classes.navbar_btn} to={'/login'}>
                        <Button className={'primary-blue'}>
                            Войти
                        </Button>
                    </Link>
                )}
            </div>
            <ul className={classes.navbar_menu}>
                <Link to={project !== null ? '/info' : '/project'}>
                    <li className={classes.navbar_item}>
                        <LineChartOutlined className={classes.navbar_item_icon}/>
                        <p className={classes.navbar_item_name}>Проект</p>
                    </li>
                </Link>
                <Link to={project !== null ? '/board' : '/settings'}>
                    <li className={classes.navbar_item}>
                        <BorderOutlined className={classes.navbar_item_icon}/>
                        <p className={classes.navbar_item_name}>Канбан доска</p>
                    </li>
                </Link>
                <Link to='/contacts'>
                    <li className={classes.navbar_item}>
                        <UsergroupAddOutlined className={classes.navbar_item_icon}/>
                        <p className={classes.navbar_item_name}>Контакты</p>
                    </li>
                </Link>
                <Link to='/chat'>
                    <li className={classes.navbar_item}>
                        <MessageOutlined className={classes.navbar_item_icon}/>
                        <p className={classes.navbar_item_name}>Чат</p>
                    </li>
                </Link>
            </ul>
            <Link to={user !== null ? '/settings' : '/login'} className={classes.navbar_footer}>
                <SettingOutlined className={classes.navbar_footer_icon}/>
                <p className={classes.navbar_footer_name}>Настройки</p>
            </Link>
            {user && (
                <Select
                    showSearch
                    placeholder="Выберите проект"
                    onChange={(value) => selectProject(value)}
                    value={selectedProject} // Установить выбранное значение
                    options={[
                        ...(user.projects?.map((project: TProject) => ({
                            value: project.id,
                            label: project.name
                        })) || []),
                        ...(projectsAsMember?.map((project: TProject) => ({
                            value: project.id,
                            label: project.name
                        })) || [])
                    ]}
                />
            )}
        </div>
    );
};

export default NavBar;
