import React from 'react';
import {ContainerProps} from '@/components/Container/type';
import classes from './style.module.scss'

const Container: React.FC<ContainerProps> = ({children}) => {
    return <div className={classes.container}>{children}</div>
};

export default Container;
