import React, {FC, useEffect, useState} from 'react';
import classes from './style.module.scss';
import {ChatProps} from "@/components/Chat/type";
import {useSelector} from "react-redux";
import {RootState} from "@/app/store/reducers/rootReducer";
import moment from "moment";
import {CloseOutlined, MoreOutlined, PaperClipOutlined, SmileOutlined} from "@ant-design/icons";
import {MessageService} from "@/helpers/utils/messageService";
import {Button, Input, message, Popconfirm, Popover} from "antd";
import {emojis} from "@/components/Chat/constants";
import {Simulate} from "react-dom/test-utils";
import cancel = Simulate.cancel;

const Chat: FC<ChatProps> = ({selectedChat, messages, sendMessage, loadMessagesForChat}) => {
    const [messageInput, setMessageInput] = useState<string>('');

    const user = useSelector((state: RootState) => state.user.user);

    const handleSendMessage = () => {
        if (messageInput.trim() !== '') {
            sendMessage(messageInput)
            setMessageInput('');
        }
    };


    const deleteMessage = (message_id: number) => {
        MessageService.messageUpdate(message_id)
            .then(() => {
                loadMessagesForChat()
                message.success('Сообщение удалено!')
            })
            .catch(() => {
                message.error('Не удалось удалить сообщение!')
            })
    }

    const handleKeyPress = (event: React.KeyboardEvent<HTMLTextAreaElement>) => {
        if (event.key === 'Enter' && !event.shiftKey) {
            event.preventDefault();
            handleSendMessage();
        }
    };

    const handleEmojiClick = (emoji: string) => {
        setMessageInput(prev => prev + emoji);
    };


    const emojiList = (
        <div className={classes.emojiGrid}>
            {emojis.map((emoji) => (
                <span key={emoji} onClick={() => handleEmojiClick(emoji)}>{emoji}</span>
            ))}
        </div>
    );

    return (
        <div className={classes.chat}>
            <header className={classes.chat_header}>
                <div className={classes.chat_header_info}>
                    <img
                        className={classes.chat_header_image}
                        src={selectedChat?.image} alt='Avatar'
                    />

                    <p>{selectedChat.first_name} {selectedChat.last_name}</p>
                </div>

                <MoreOutlined className={classes.chat_header_more}/>
            </header>
            <div className={classes.chat_content}>
                {messages?.map((message) => (
                    <div key={message.id}
                         className={`${classes.message} 
                         ${message.author === user?.id
                             ? classes.myMessage
                             : ''
                         }`}>
                        <div
                            className={message.author === user?.id
                                ? classes.message_myinfo
                                : classes.message_userinfo}
                        >
                            <img className={classes.message_image}
                                 src={message.author === user?.id
                                     ? user?.image
                                     : selectedChat?.image}
                                 alt='Avatar'
                            />
                            <p
                                className={classes.author}>
                                {message.author === user?.id
                                    ? 'Вы'
                                    : `${selectedChat?.first_name} ${selectedChat?.last_name}`}
                            </p>
                            <p className={classes.message_timestamp}>{moment(message.timestamp).format('HH:MM')}</p>
                        </div>
                        <div className={message.author === user?.id ?
                            classes.message_mycontent :
                            classes.message_usercontent
                        }>
                            <p>{message.content}</p>
                            {user?.id === message.author && (
                                <>


                                    <Popconfirm
                                        title="Удалить сообщение"
                                        description="Вы точно хотите удалить сообщение?"
                                        onConfirm={() => deleteMessage(message.id)}
                                        okText="Да"
                                        cancelText="Нет"
                                        placement="left"
                                    >
                                        <CloseOutlined
                                            className={classes.delete}
                                        />
                                    </Popconfirm>
                                </>
                            )}
                        </div>
                    </div>
                ))}
            </div>
            <div className={classes.chat_footer}>
                <Input.TextArea
                    showCount
                    autoSize={{minRows: 1, maxRows: 2}}
                    maxLength={300}
                    className={classes.chat_footer_input}
                    placeholder="Напишите сообщение..."
                    value={messageInput}
                    onChange={(e) => setMessageInput(e.target.value)}
                    onKeyPress={handleKeyPress}
                />
                <div className={classes.chat_footer_other}>
                    <PaperClipOutlined className={classes.chat_footer_icon}/>
                    <Popover
                        placement="top"
                        content={emojiList}
                        trigger="click"
                    >
                        <SmileOutlined className={classes.chat_footer_icon}/>
                    </Popover>
                </div>
            </div>
        </div>
    );
};

export default Chat;