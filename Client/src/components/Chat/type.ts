import {TUser} from "@/helpers/types/authTypes";
import {TMessage} from "@/helpers/types/messageTypes";

export interface ChatProps {
    selectedChat: TUser | null;
    messages: TMessage[];
    sendMessage: (content: string) => void;
    loadMessagesForChat: () => void
}