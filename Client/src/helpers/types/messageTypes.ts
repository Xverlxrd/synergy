import {TProject} from "@/helpers/types/projectType";

export type TMessage = {
    id: number;
    author: number;
    content: string;
    timestamp: string;
    recipient: number;
};

export type TMessageData = {
    author: number;
    content: string;
    recipient: number;
}