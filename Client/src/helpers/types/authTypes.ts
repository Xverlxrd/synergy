import {TProject} from "@/helpers/types/projectType";

export type TAuthResponse = {
    access_token: string;
    token_type: string;
    user_name: string;
    user_login: string;
    user_role: string;
    expires_in: number;
};

export type TUser = {
    id: number;
    about: string;
    contacts: number[];
    date_joined: string;
    email: string;
    first_name: string;
    groups: [];
    image: string;
    is_active: boolean;
    is_staff: boolean;
    is_superuser: boolean;
    job_title: string;
    last_login: string;
    last_name: string;
    projects: TProject[];
    role: string;
    skills: string;
    user_permissions: [];
    username: string;
    work_experience: string;
};

export type TUserData = {
    exp: number;
    iat: number;
    iss: string;
    jti: string;
    nbf: number;
    prv: string;
    role: string;
    sub: string;
    user: TUser;
};

export type TAuthData = {
    username: string,
    password: string
}

export type TRegData = {
    username: string,
    password: string,
    first_name: string,
    last_name: string,
    email: string
}

export type TUpdateData = {
    skills: string,
    job_title: string,
    work_experience: string,
    about: string,
    image: TUserAvatar | null;
}

export type TUserAvatar = {
    file: File,
    fileList: []
}