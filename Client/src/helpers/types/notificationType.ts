export type TNotification = {
    id: number,
    message: string,
    created_at?: string,
    is_read?: boolean,
    is_system?: boolean,
    users: number[],
    project?: number | null,
    members?: number[]
    created_by?: number
};

export type TNotificationData = {
    message: string,
    is_system?: boolean,
    users: number[],
    project?: number | null
    members?: number[]
    created_by?: number
}