export type TProject = {
    isEnd: boolean;
    id: number,
    members: number[],
    name: string,
    description: string,
    start_date: string,
    end_date: string,
    created_by: number
};

export type TProjectData = {
    "name": string,
    "description"?: string,
    "created_by": number,
    "end_date"?: string,
    "members"?: []
}

export type TColumn = {
    id: number,
    name: string,
    project: number
}

export type TColumnData = {
    "name": string,
    "project": number
}

export type TTask = {
    "id"?: number,
    "title"?: string,
    "description"?: string,
    "created_at"?: string,
    "due_date"?: string,
    "completed"?: boolean,
    "priority"?: string,
    "project"?: number,
    "column": number,
    "assigned_to"?: null
}

export type TTaskData = {
    title: string
}