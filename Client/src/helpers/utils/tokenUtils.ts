import {setAuthToken} from "@/helpers/utils/axiosConfig";
import {NavigateFunction} from "react-router-dom";
import {jwtDecode} from "jwt-decode";
import {HookAPI} from "antd/es/modal/useModal";
import {AuthService} from "@/helpers/utils/authService";

export const checkTokenExpiration = async (setUser: any, navigate: NavigateFunction, modal: HookAPI) => {
    const access_token = localStorage.getItem('access_token');
    const refresh_token = localStorage.getItem('refresh_token');

    if (access_token) {
        const tokenDecode = jwtDecode(access_token);
        const currentTime = Math.floor(Date.now() / 1000);
        const tokenExpiration = tokenDecode.exp;

        if (tokenExpiration - 299 < currentTime) {
            setAuthToken(null);
            if (refresh_token) {
                try {
                    AuthService.refresh()
                        .then((response) => {
                            setAuthToken(response.access);
                        })
                        .catch(error => {
                            console.error('Ошибка при обновлении токена:', error);
                            setUser(null)
                            navigate('/login');
                        })
                } catch (error) {
                    console.error('Ошибка при обновлении токена:', error);
                    setUser(null)
                    navigate('/login');
                }
            } else {
                navigate('/login');
            }
        }
    }
};