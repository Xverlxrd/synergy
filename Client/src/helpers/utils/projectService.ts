import axios, {AxiosResponse} from "axios";
import {instance as axiosInstance, setAuthToken} from "@/helpers/utils/axiosConfig";
import {TColumn, TColumnData, TProject, TProjectData, TTask, TTaskData} from "@/helpers/types/projectType";
import {TUser} from "@/helpers/types/authTypes";
import {jwtDecode} from "jwt-decode";
export class ProjectService {
    static async createProject(values: TProjectData): Promise<AxiosResponse> {
        return await axiosInstance.post(`${__API_URL__}/projects/create/`, values)
    }

    static async projectInfo(projectId:number): Promise<AxiosResponse<TProject>> {
        const token = localStorage.getItem('access_token');
        if (token) {
            setAuthToken(token);
            return await axiosInstance.get(`${__API_URL__}/projects/${projectId}/`);
        } else {
            throw new Error('Токен не найден');
        }

    }

    static async allProjects(): Promise<AxiosResponse<TProject[]>> {
        const token = localStorage.getItem('access_token');
        const decodedToken:any = jwtDecode(token);
        if (token) {
            setAuthToken(token);
            return await axiosInstance.get(`${__API_URL__}/projects/`);
        } else {
            throw new Error('Токен не найден');
        }

    }

    static async updateProject(project_id: number , members?: number[], isEnd?: boolean): Promise<AxiosResponse> {
        const token = localStorage.getItem('access_token');
        if (token) {
            setAuthToken(token);
            return await axiosInstance.patch(`${__API_URL__}/projects/${project_id}/`, { members, isEnd });
        } else {
            throw new Error('Токен не найден');
        }

    }

    static async createColumn(values: TColumnData, project_id: number): Promise<AxiosResponse> {
        return await axiosInstance.post(`${__API_URL__}/projects/${project_id}/columns/`, values)
    }

    static async getColumns(project_id:number): Promise<AxiosResponse<TColumn[]>> {
        const token = localStorage.getItem('access_token');
        if (token) {
            setAuthToken(token);
            return await axiosInstance.get(`${__API_URL__}/projects/${project_id}/columns/`);
        } else {
            throw new Error('Токен не найден');
        }

    }

    static async deleteColumns(column_id:number): Promise<AxiosResponse> {
        const token = localStorage.getItem('access_token');
        if (token) {
            setAuthToken(token);
            return await axiosInstance.delete(`${__API_URL__}/columns/${column_id}/`);
        } else {
            throw new Error('Токен не найден');
        }

    }

    static async updateColumns(column_id:number, name: string): Promise<AxiosResponse> {
        const token = localStorage.getItem('access_token');
        if (token) {
            setAuthToken(token);
            return await axiosInstance.patch(`${__API_URL__}/columns/${column_id}/`, name);
        } else {
            throw new Error('Токен не найден');
        }

    }

    static async createTask(values: TTaskData): Promise<AxiosResponse> {
        return await axiosInstance.post(`${__API_URL__}/tasks/create`, values)
    }

    static async getTask(project_id:number): Promise<AxiosResponse<TTask[]>> {
        const token = localStorage.getItem('access_token');
        if (token) {
            setAuthToken(token);
            return await axiosInstance.get(`${__API_URL__}/projects/${project_id}/tasks/`);
        } else {
            throw new Error('Токен не найден');
        }

    }

    static async deleteTask(task_id:number): Promise<AxiosResponse> {
        const token = localStorage.getItem('access_token');
        if (token) {
            setAuthToken(token);
            return await axiosInstance.delete(`${__API_URL__}/tasks/${task_id}/`);
        } else {
            throw new Error('Токен не найден');
        }

    }

    static async updateTask(task_id:number, values: TTask): Promise<AxiosResponse> {
        const token = localStorage.getItem('access_token');
        if (token) {
            setAuthToken(token);
            return await axiosInstance.patch(`${__API_URL__}/tasks/${task_id}/`, values);
        } else {
            throw new Error('Токен не найден');
        }

    }

    static async getTaskInfo(task_id:number): Promise<AxiosResponse> {
        const token = localStorage.getItem('access_token');
        if (token) {
            setAuthToken(token);
            return await axiosInstance.get(`${__API_URL__}/tasks/${task_id}/`);
        } else {
            throw new Error('Токен не найден');
        }

    }
}