import axios, {AxiosResponse} from "axios";
import {instance as axiosInstance, setAuthToken} from '@/helpers/utils/axiosConfig';
import {TUser, TUserData} from "@/helpers/types/authTypes";
import {jwtDecode} from "jwt-decode";

export class AuthService {

    static async login(username: string, password: string): Promise<AxiosResponse> {
        return await axios.post(`${__API_URL__}/login/`, {username, password});
    }

    static async registration(username: string, password: string, first_name: string, last_name: string, email: string): Promise<AxiosResponse> {
        return await axiosInstance.post(`${__API_URL__}/users/`, {username, password, first_name, last_name, email})
    }

    static async refresh(): Promise<{ access: string }> {
        const refreshToken = localStorage.getItem('refresh_token');
        try {
            if (refreshToken) {
                const response = await axiosInstance.post('login/refresh/', { refresh: refreshToken }, {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });

                return response.data;
            }
        } catch (error) {
            console.error('Ошибка во время выхода!', error);
            throw error;
        }
    }
    static async logout(): Promise<void> {
        const refreshToken = localStorage.getItem('refresh_token');

        try {
            if (refreshToken) {
                const response = await axiosInstance.post('logout/', { refresh: refreshToken }, {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
            }
        } catch (error) {
            console.error('Ошибка во время выхода!', error);
        } finally {
            localStorage.removeItem('refresh_token');
            localStorage.removeItem('access_token');
        }
    }

    static async me(): Promise<AxiosResponse<TUser>> {
        const token = localStorage.getItem('access_token');
        const decodedToken:any = jwtDecode(token);
        if (token) {
            setAuthToken(token);
            return await axiosInstance.get(`${__API_URL__}/users/${decodedToken.user_id}/`);
        } else {
            throw new Error('Токен не найден');
        }

    }

    static async allUsers(): Promise<AxiosResponse<TUser[]>> {
        const token = localStorage.getItem('access_token');
        const decodedToken:any = jwtDecode(token);
        if (token) {
            setAuthToken(token);
            return await axiosInstance.get(`${__API_URL__}/users/`);
        } else {
            throw new Error('Токен не найден');
        }

    }

    static async update(values:FormData): Promise<AxiosResponse<TUser>> {
        const token = localStorage.getItem('access_token');
        const decodedToken:any = jwtDecode(token);
        if (token) {
            setAuthToken(token);
            return await axiosInstance.patch(`${__API_URL__}/usersdetail/${decodedToken.user_id}/`, values)
        } else {
            throw new Error('Токен не найден');
        }
    }

    static async addUser(user_id_to_add: number): Promise<AxiosResponse<TUser>> {
        const token = localStorage.getItem('access_token');
        const decodedToken:any = jwtDecode(token);
        if (token) {
            setAuthToken(token);
            return await axiosInstance.post(`${__API_URL__}/add_contact/`, {user_id_to_add})
        } else {
            throw new Error('Токен не найден');
        }
    }

    static async deleteUser(user_id_to_remove: number): Promise<AxiosResponse<TUser>> {
        const token = localStorage.getItem('access_token');
        const decodedToken:any = jwtDecode(token);
        if (token) {
            setAuthToken(token);
            return await axiosInstance.post(`${__API_URL__}/remove_contact/`, {user_id_to_remove})
        } else {
            throw new Error('Токен не найден');
        }
    }
}
