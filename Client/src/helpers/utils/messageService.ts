import {AxiosResponse} from "axios";
import {TUser} from "@/helpers/types/authTypes";
import {jwtDecode} from "jwt-decode";
import {instance as axiosInstance, setAuthToken} from "@/helpers/utils/axiosConfig";
import {TMessage, TMessageData} from "@/helpers/types/messageTypes";
import {TTaskData} from "@/helpers/types/projectType";

export class MessageService {
    static async allMessages(): Promise<AxiosResponse<TMessage[]>> {
        const token = localStorage.getItem('access_token');
        const decodedToken:any = jwtDecode(token);
        if (token) {
            setAuthToken(token);
            return await axiosInstance.get(`${__API_URL__}/messages/`);
        } else {
            throw new Error('Токен не найден');
        }

    }

    static async createMessage(values: TMessageData): Promise<AxiosResponse> {
        return await axiosInstance.post(`${__API_URL__}/messages/`, values)
    }

    static async messageUpdate(message_id: number) {
        const token = localStorage.getItem('access_token');
        const decodedToken:any = jwtDecode(token);
        if (token) {
            setAuthToken(token);
            return await axiosInstance.delete(`${__API_URL__}/messages/${message_id}/`);
        } else {
            throw new Error('Токен не найден');
        }

    }

}