import {TProjectData} from "@/helpers/types/projectType";
import {AxiosResponse} from "axios";
import {instance as axiosInstance, setAuthToken} from "@/helpers/utils/axiosConfig";
import {TNotification, TNotificationData} from "@/helpers/types/notificationType";
import {TUser} from "@/helpers/types/authTypes";
import {jwtDecode} from "jwt-decode";

export class NotificationService {
    static async createNotification(values: TNotificationData): Promise<AxiosResponse> {
        return await axiosInstance.post(`${__API_URL__}/notifications/`, values)
    }

    static async allNotifications(): Promise<AxiosResponse<TNotification[]>> {
        const token = localStorage.getItem('access_token');
        const decodedToken:any = jwtDecode(token);
        if (token) {
            setAuthToken(token);
            return await axiosInstance.get(`${__API_URL__}/notifications/`);
        } else {
            throw new Error('Токен не найден');
        }

    }

    static async deleteNotification(notification_id:number): Promise<AxiosResponse> {
        const token = localStorage.getItem('access_token');
        if (token) {
            setAuthToken(token);
            return await axiosInstance.delete(`${__API_URL__}/notifications/${notification_id}/`);
        } else {
            throw new Error('Токен не найден');
        }

    }
}